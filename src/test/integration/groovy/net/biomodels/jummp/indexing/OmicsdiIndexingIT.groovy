/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing

import net.biomodels.jummp.core.model.ModelState
import net.biomodels.jummp.indexing.exporter.OmicsdiDataSetEntry
import net.biomodels.jummp.utils.ExporterUtils
import net.biomodels.jummp.indexing.exporter.OmicsdiXmlBasedSchemaExporter
import net.biomodels.jummp.model.Revision
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.transaction.annotation.Transactional

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertTrue

/**
 * @author Tung Nguyen <tung.nguyen@ebi.ac.uk>
 */
@RunWith(JUnit4.class)
@Transactional
class OmicsdiIndexingIT {
    private static final Logger logger = LoggerFactory.getLogger(OmicsdiIndexingIT.class)

    def url = 'jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000;DB_CLOSE_ON_EXIT=FALSE'
    def u = 'sa'
    def pwd = ''
    def personSettings = [
        name: 'me',
        institution:  'somewhere LTD',
        username:  "root",
        email: "me@example.com"
    ]
    def formatSettings = [name: "SBML", identifier: "SBML", formatVersion: "L2V4"]

    OmicsdiXmlBasedSchemaExporter generator

    @Test
    void testExportingXMLFromSBMLModel() {
        OmicsdiDataSetEntry e = new OmicsdiDataSetEntry()
        e.id = "0001"
        e.name = "Tung2017_Model_Sample"
        e.tokenisedName = ExporterUtils.tokenise(e.name)
        e.description = "This is an model sample used for testing."
        e.dates = ["publication": new Date(),
                   "submission": new Date(),
                   "modification": new Date()
        ]
        e.omicsType = "Proteomics"
        e.authors = new ArrayList<String>()
        e.authors.add("Tung Nguyen")
        e.authors.add("Mihai Glont")
        e.publicationYear = 1999
        // this is BIOMD0000000002, derived from BIOMD0000000001
        e.nonIsDerivedFromBioModelsAnnotations = ["BIOMD0000000002": "biomodels.db"]
        e.crossReferences = [
            "BIOMD0000000001": "biomodels.db",
            "9606": "taxonomy",
        ]
        assertNotNull(e)

        List<OmicsdiDataSetEntry> entries = new ArrayList<OmicsdiDataSetEntry>()
        entries.add(e)
        assertTrue(entries.size() > 0)
        generator = new OmicsdiXmlBasedSchemaExporter()
        def time = System.nanoTime()
        String res = generator.buildSchemaXmlAsString(entries)
        logger.info res
        time = (System.nanoTime() - time) / 1000000.0
        logger.debug "It took ${time}ms to build xml string."
        assertTrue(res.size() > 0)
        assertTrue res.contains("<field name=\"non_derived_xrefs\">BIOMD0000000002 biomodels.db</field>")
        assertTrue res.contains("<ref dbkey=\"BIOMD0000000001\" dbname=\"biomodels.db\" />")
        assertTrue res.contains("<field name=\"tokenised_name\">Tung2017 Model Sample</field>")
        assertFalse res.contains("<tokenised_name>Tung2017 Model Sample</tokenised_name>")
    }

    @Test
    void testBuildingOmicsDIEntries() {
        // create model and revisions
        def ctx = TestUtils.buildMockRequestContextForDatabase(url, u, pwd)
        generator = new OmicsdiXmlBasedSchemaExporter()
        def defaultBuilderArgs = [
            person: personSettings,
            format: formatSettings
        ]

        def scaffoldModel = TestUtils.scaffoldModel()
        def builderArgs = defaultBuilderArgs +
            [revision: [state: ModelState.PUBLISHED]]
        Revision.withNewSession {
            Revision revision = TestUtils.buildMockRevision(builderArgs, scaffoldModel)
            assertNotNull TestUtils.persistRevision(revision)

            // these annotations should be included in the crossReferences map
            TestUtils.addModelLevelAnnotation("http://biomodels.net/model-qualifiers/isDescribedBy",
                "http://identifiers.org/pubmed/12345678", revision)
            TestUtils.addModelLevelAnnotation("http://biomodels.net/model-qualifiers/isDerivedFrom",
                "http://identifiers.org/biomodels.db/BIOMD0000001000", revision)
            TestUtils.addModelLevelAnnotation("http://biomodels.net/model-qualifiers/isDerivedFrom",
                "http://identifiers.org/pubmed/87654321", revision)
            TestUtils.addModelLevelAnnotation("http://biomodels.net/biology-qualifiers/hasTaxon",
                "http://identifiers.org/taxonomy/9606", revision)
            TestUtils.addModelLevelAnnotation("http://biomodels.net/biology-qualifiers/hasProperty",
                "http://identifiers.org/pato/PATO:0000383", revision)

            // this should go in nonIsDerivedFromBioModelsAnnotations
            TestUtils.addModelLevelAnnotation("http://biomodels.net/model-qualifiers/is",
                "http://identifiers.org/biomodels.db/MODEL1234567890", revision)

            // create list of OmicsDI entries
            List<Revision> revisions = generator.selectModelRevisionsForExport([scaffoldModel])
            List<OmicsdiDataSetEntry> entries = generator.convertToListOfDataSetEntries(revisions, ctx)

            assertEquals(1, entries.size())
            def firstEntry = entries.first()
            Map nonIsDerivedFromAnnotations = firstEntry.nonIsDerivedFromBioModelsAnnotations

            assertEquals(["MODEL1234567890"], nonIsDerivedFromAnnotations.keySet().toList())
            def xrefs = firstEntry.crossReferences
            assertEquals 5, xrefs?.size()
            logger.debug "...verified ${entries.size()} OmicsDI entries."
        }
    }
}
