/**
 * Copyright (C) 2010-2016 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing

import net.biomodels.jummp.core.model.ModelState
import net.biomodels.jummp.model.Model
import net.biomodels.jummp.model.ModelFormat
import net.biomodels.jummp.model.Revision
import net.biomodels.jummp.plugins.security.Person
import net.biomodels.jummp.plugins.security.User

/**
 * Simple class for creating fully-populated revision objects.
 *
 * The typical usage of this class is
 *
 *   Revision revision = new MockRevisionBuilder()
 *     .withPerson(personSettings)
 *     .withFormat(formatSettings)
 *     .build(revisionSettings, model)
 * where personSettings, formatSettings and revisionSettings are property maps. Consult each method
 * for the specific format of each map in terms of expected keys.
 *
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 * @author Tung Nguyen <tung.nguyen@ebi.ac.uk>
 */

class MockRevisionBuilder {
    Revision revision = new Revision()

    MockRevisionBuilder withFormat(Map args) {
        String name = args?.name ?: "Unusual Format"
        String identifier = args?.identifier ?: "UNKNOWN"
        String version = args?.version ?: "version X"

        def format = ModelFormat.findOrCreateWhere(name: name, identifier: identifier, formatVersion: version)
        revision.format = format
        this
    }

    MockRevisionBuilder withPerson(Map args) {
        String name = args?.name ?: "Mary Roe"
        String institution = args?.institution ?: "Somewhere LTD"
        String username = args?.username ?: "root"
        String email = args?.email ?: "mary@example.com"

        def person = Person.findOrCreateWhere(userRealName: name, institution: institution)
        def user = User.findOrCreateWhere(username: username, email: email,
            passwordExpired: false, password: 'obscure', accountExpired: false,
            accountLocked: false, enabled: true)
        // cannot use the person attribute in the user lookup above as it may have not been saved yet.
        user.person = person
        revision.owner = user
        return this
    }

    Revision build(Map args, Model model = null) {
        String revisionHash = args?.revisionHash ?: "abcdef1234"
        int revisionNumber = args?.revisionNumber ?: 1
        String name = args?.name ?: "Revision $revisionHash"
        Date uploadDate = args?.uploadDate ?: new Date()
        boolean isMinorRevision = args?.isMinorRevision ?: false
        ModelState state = args?.state ?: ModelState.UNPUBLISHED

        revision.vcsId = revisionHash
        revision.revisionNumber = revisionNumber
        revision.minorRevision = isMinorRevision
        revision.name = name
        revision.uploadDate = uploadDate
        revision.state = state

        if (model) {
            revision.model = model
            model.addToRevisions revision
        }
        revision
    }
}
