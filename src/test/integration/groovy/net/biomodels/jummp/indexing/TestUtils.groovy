/**
 * Copyright (C) 2010-2016 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing

import net.biomodels.jummp.annotationstore.ElementAnnotation
import net.biomodels.jummp.annotationstore.Qualifier
import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.annotationstore.RevisionAnnotation
import net.biomodels.jummp.annotationstore.Statement
import net.biomodels.jummp.model.Model
import net.biomodels.jummp.model.ModelElementType
import net.biomodels.jummp.model.ModelFormat
import net.biomodels.jummp.model.Revision
import net.biomodels.jummp.plugins.security.Person
import net.biomodels.jummp.plugins.security.User
import org.springframework.transaction.annotation.Transactional

import static org.junit.Assert.*

/**
 * This class defines methods helping us initialise necessary environments for running test cases.
 *
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 * @author Tung Nguyen <tung.nguyen@ebi.ac.uk>
 */

@Transactional
final class TestUtils {
    /**
     * Convenience method for bootstrapping an integration test.
     *
     * Uses the supplied JSON file to construct a RequestContext. The context's configFilePath
     * will be set to the the actual path to .jummp.properties, which is fetched in the
     * usual way. A model and a revision (having the supplied @p format) are persisted to the
     * database to ensure that the request context being built references the correct model and
     * revision identifiers.
     *
     * @param jsonFile the indexData.json from which to extract contextual information
     * @param format the format of the model associated with this requestContext
     * @return a fully-populated RequestContext
     */

    private static final String BQ_MODEL_NS = "http://biomodels.net/model-qualifiers/"
    private static final String BQ_BIOL_NS = "http://biomodels.net/biology-qualifiers/"
    // exclude http|https from the marker so that we can cope with both schemes
    private static final String IDENTIFIERS_ORG_URI  = "//identifiers.org/"

    static RequestContext buildMockRequestContext(File jsonFile, ModelFormat format,
            Set formatElementTypes = [] as Set) {
        RequestContext ctx = new RequestContext(jsonFile.absolutePath)
        assertFalse ctx.isEmpty()
        String jummpPropertiesPath = System.getenv("JUMMP_CONFIG")
        if (!jummpPropertiesPath) {
            String home = System.getProperty('user.home')
            jummpPropertiesPath = "$home${File.separator}.jummp.properties"
        }
        ctx.configFilePath = jummpPropertiesPath
        GormUtil.isTestEnvironment = true
        GormUtil.initGorm(ctx)
        GormUtil.openSession()


        Person p = new Person(userRealName: 'not me', institution: "M.E. PLC")
        Revision.withNewSession {
            assertNotNull(p.save())
            assertFalse(p.hasErrors())

            User self = User.findByEmail 'my@self.name'
            if (!self) {
                self = new User(username: 'me', email: 'my@self.name', person: p,
                    passwordExpired: false, password: 'obscure', accountExpired: false,
                    accountLocked: false, enabled: true)

                assertNotNull(self.save())
                assertFalse(self.hasErrors())
            }
            format = ModelFormat.findOrSaveByIdentifierAndFormatVersionAndName(format.identifier,
                    format.formatVersion, format.name)
            if (!format.id) {
                assertNotNull(format.save())
                assertFalse(format.hasErrors())
            }
            Model model = Model.findBySubmissionId "MODEL00000400"
            if (!model) {
                model = new Model(vcsIdentifier: "test/", submissionId: "MODEL00000400")
            }
            ModelElementTypeUtils.createForFormatAndElements(format, formatElementTypes)

            def revision = model.id ? Revision.findByModelAndRevisionNumber(model, 1) : null
            if (!revision) {
                revision = new Revision(model: model, vcsId: "1", revisionNumber: 1,
                    owner: self, minorRevision: false, name: "interesting model v1",
                    uploadDate: new Date(), format: format)

                /*
                 * Normally, we would now call
                 *     model.addToRevisions(revision)
                 * but until we upgrade to GORM v4, there is a problem that precludes
                 * dynamic methods from being added to the metaClass of domain objects
                 * in applications that only use GORM, rather than the full Grails stack.
                 *
                 * One workaround is to employ DefaultGroovyMethods.setProperty(), which is
                 * what we've done here, another would be to use the meta class of
                 * HibernateUtils.groovy to call gormEnhancer.enhance(entity, false), just like
                 * the patch below does.
                 *
                 * @see https://github.com/grails/grails-data-mapping/commit/ddf8e181
                 */
                model.revisions = [revision]
                assertNotNull(model.save(flush: true))
                assertFalse(model.hasErrors())

                assertTrue(revision.validate())
                assertNotNull(revision.save(flush: true))
                assertFalse(revision.hasErrors())
            }

            ctx.partialData['model_id'] = revision.model.id
            ctx.partialData['revision_id'] = revision.id
            AnnotationReferenceResolver.initialiseWithProperties(jummpPropertiesPath)
        }
        ctx
    }
    /**
     * Static factory method for building a {@link MockRequestContext} with the desired database settings.
     *
     * This method initialises GORM for the test environment and populates the application context with the
     * beans that are needed by GORM.
     *
     * @param url the url of the database connection
     * @param user the account that should be used to connect to the database
     * @param password the password of the account that should be used to connect to the database
     * @return a MockRequestContext
     */
    static MockRequestContext buildMockRequestContextForDatabase(String url, String user, String password) {
        def ctx = new MockRequestContext(url, user, password)
        GormUtil.isTestEnvironment = true
        GormUtil.initGorm(ctx)
        ctx
    }

    static Model scaffoldModel(String vcsIdentifier = "aaa/MODEL00001/",
            String submissionId = "MODEL00001") {
        new Model(vcsIdentifier:vcsIdentifier, submissionId: submissionId)
    }

    static Revision buildMockRevision(Map args, Model model = null) {
        def personSettings = args?.person ?: [:]
        def formatSettings = args?.format ?: [:]
        def revisionSettings = args?.revision ?: [:]
        new MockRevisionBuilder()
            .withPerson(personSettings)
            .withFormat(formatSettings)
            .build(revisionSettings, model)
    }

    static Revision persistRevision(Revision revision) {
        revision.format.save()
        revision.owner.person.save()
        revision.owner.save()
        revision.model.save()
        revision
    }

    static RevisionAnnotation addModelLevelAnnotation(String qualifierUri, String xref, Revision r) {
        addAnnotationForElement("#", "model", qualifierUri, xref, r)
    }

    static RevisionAnnotation addAnnotationForElement(String subjectId, String elementType, String qualifierUri, String xref, Revision r) {
        if (!r.id) throw new IllegalArgumentException("Save the revision before calling this method")

        Qualifier q  = newQualifier(qualifierUri)
        ResourceReference o = newResourceReference(xref)
        Statement statement = Statement.findOrSaveWhere(subjectId: subjectId, qualifier: q, object: o)
        String creator = r.owner.person.getUserRealName()
        ModelElementType modelElementType = ModelElementType.findOrSaveWhere(name: elementType, modelFormat: r.format)
        ElementAnnotation ea = ElementAnnotation.findOrSaveWhere(creatorId: creator, statement: statement,
            modelElementType: modelElementType)
        def ra = RevisionAnnotation.get(r.id, ea.id) ?: RevisionAnnotation.create(r, ea, true)
        ra
    }

    private static ResourceReference newResourceReference(String uri) {
        String datatype = detectDataTypeForXref(uri)
        ResourceReference.findOrSaveWhere(uri: uri, datatype: datatype)
    }

    private static String detectDataTypeForXref(String uri) {
        String datatype
        int DATATYPE_MARKER_START = uri.indexOf(IDENTIFIERS_ORG_URI) + IDENTIFIERS_ORG_URI.length()
        if (DATATYPE_MARKER_START < IDENTIFIERS_ORG_URI.length()) { // this is not an identifiers.org URI, bail out
            datatype = "unknown"
        } else {
            int DATATYPE_MARKER_END = uri.indexOf('/', DATATYPE_MARKER_START)
            datatype = uri.substring(DATATYPE_MARKER_START, DATATYPE_MARKER_END)
        }
        datatype
    }

    private static Qualifier newQualifier(String uri) {
        if (uri?.trim()?.isEmpty()) throw new IllegalArgumentException("RTFM: http://co.mbine.org/standards/qualifiers")
        if (uri.startsWith(BQ_MODEL_NS))
            return Qualifier.findOrSaveWhere(uri:  uri, qualifierType: BQ_MODEL_NS)
        else if (uri.startsWith(BQ_BIOL_NS))
            return Qualifier.findOrSaveWhere(uri:  uri, qualifierType: BQ_BIOL_NS)
        else
            throw new IllegalArgumentException("RTFM: http://co.mbine.org/standards/qualifiers")
    }
}
