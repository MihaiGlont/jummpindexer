/**
 * Copyright (C) 2010-2016 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing

/**
 * Simple mock for {@link RequestContext} to help unit testing.
 *
 * It allows you to set up a database connection and to bootstrap GORM without
 * having to create a fully-populated RequestContext from a JSON file.
 *
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 * @author Tung Nguyen <tung.nguyen@ebi.ac.uk>
 */

class MockRequestContext extends RequestContext {
    /**
     * Constructs a RequestContext with values defined in the given @p documentPath.
     */
    MockRequestContext(String documentPath) {
        super(documentPath)
    }

    MockRequestContext(String url, String user, String pwd) {
        super()
        databaseURL = url
        databaseUsername = user
        databasePassword = pwd
    }
}
