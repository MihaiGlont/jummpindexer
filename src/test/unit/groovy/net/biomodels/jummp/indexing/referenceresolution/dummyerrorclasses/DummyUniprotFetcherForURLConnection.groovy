package net.biomodels.jummp.indexing.referenceresolution.dummyerrorclasses


import net.biomodels.jummp.indexing.NonCachingUniProtFetcher

/**
 * @author carankalle on 03/10/2019.
 */
class DummyUniprotFetcherForURLConnection {
    static NonCachingUniProtFetcher newInstance() {
        new NonCachingUniProtFetcher(){
            @Override
            def parseXMLText(String r){
                throw new IOException()
            }
        }
    }
}
