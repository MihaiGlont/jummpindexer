/**
 * Copyright (C) 2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/
package net.biomodels.jummp.indexing.referenceresolution

import net.biomodels.jummp.annotationstore.ResolutionStatus
import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.indexing.referenceresolution.dummyerrorclasses.DummyKeggFetcherForURLConnection

/**
 * @author carankalle on 23/09/2019.
 */
class TestUtils {

    def fetcher

    TestUtils(def fetcher) {
        this.fetcher = fetcher
    }

    private static ResourceReference getResourceReferenceForTest(String accession) {
        ResourceReference resourceReference = new ResourceReference()
        resourceReference.setAccession(accession)
        return resourceReference;
    }

    private static ResourceReference getResourceReferenceForTest(String accession, String datatype) {
        ResourceReference resourceReference = new ResourceReference()
        resourceReference.setAccession(accession)
        resourceReference.setDatatype(datatype)
        return resourceReference;
    }

    private void executeTestWithResourceReference(ResourceReference resourceReference, String testName, Enum testResolutionStatus) {
        When: "Executed getTermInformation() of Fetcher class"
        ResourceReference modifiedResourceReference = fetcher.getTermInformation(resourceReference)

        Then: "It gives correct results"
        assert testName == modifiedResourceReference.name
        assert modifiedResourceReference.resolutionStatus == testResolutionStatus
    }

    void testFetcherWithAccessionAndDataType(String accession, String testName, Enum testResolutionStatus, String datatype) {
        Given: "ResourceReference object with an accession"
        ResourceReference resourceReference = getResourceReferenceForTest(accession, datatype)

        executeTestWithResourceReference(resourceReference, testName, testResolutionStatus)
    }

    void testFetcherWithAccession(String accession, String testName, ResolutionStatus testResolutionStatus) {
        Given: "ResourceReference object with an accession"
        ResourceReference resourceReference = getResourceReferenceForTest(accession)
        executeTestWithResourceReference(resourceReference, testName, testResolutionStatus)
    }

    void testFetcherWithDummyObjectForResolverError(ResourceReference myXref, def service) {
        when: "when ResourceReference object is updated through getTermInformation"
        service.getTermInformation(myXref)
        then: "It should modify ResolutionStatus of the ResourceReference object to RESOLVER_ERROR "
        assert myXref.getResolutionStatus() == ResolutionStatus.RESOLVER_ERROR
    }

    void testFetcherWithDummyObjectForUnresolvable(ResourceReference myXref, def service) {
        when: "when ResourceReference object is updated through getTermInformation"
        service.getTermInformation(myXref)
        then: "It should modify ResolutionStatus of the ResourceReference object to UNRESOLVABLE "
        assert myXref.getResolutionStatus() == ResolutionStatus.UNRESOLVABLE
    }

    static URLConnection openURLConnectionForFetcher(URL url) {
        URLConnection urlConnection = url.openConnection()
        urlConnection.responseCode = 501
        return urlConnection
    }
}
