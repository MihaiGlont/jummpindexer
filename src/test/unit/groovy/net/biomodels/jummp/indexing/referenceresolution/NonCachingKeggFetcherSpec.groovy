/**
 * Copyright (C)2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/
package net.biomodels.jummp.indexing.referenceresolution

import net.biomodels.jummp.annotationstore.ResolutionStatus
import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.indexing.NonCachingKeggFetcher
import net.biomodels.jummp.indexing.referenceresolution.dummyerrorclasses.DummyKeggFetcherForURLConnection
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


/**
 * @author Chinmay Arankalle <carankalle@ebi.ac.uk>
 */
@RunWith(JUnit4.class)
class NonCachingKeggFetcherSpec {

    TestUtils testUtils

    @Before
    void setUp() {
        testUtils = new TestUtils(new NonCachingKeggFetcher())
    }

    @Test
    void "test Kegg fetcher function for UNRESOLVABLE test"() {
        given: "ResourceReference object with C00562"
        def myXref = new ResourceReference(accession: "C00562", datatype: "kegg", uri: "http://rest.kegg.jp/get/C00562")
        def service = DummyKeggFetcherForURLConnection.newInstance()
        testUtils.testFetcherWithDummyObjectForUnresolvable(myXref, service)
    }

    @Test
    void "test Kegg database fetcher function for UNRESOLVABLE test"() {
        testUtils.testFetcherWithAccession("C05241", null, ResolutionStatus.UNRESOLVABLE)
    }

    @Test
    void "test Kegg database fetcher function for RESOLVED test of Kegg.compound"() {
        testUtils.testFetcherWithAccession("C00562", "Phosphoprotein", ResolutionStatus.RESOLVED)
    }

    @Test
    void "test Kegg database fetcher function for RESOLVED test of Kegg.pathway"() {
        testUtils.testFetcherWithAccession("ko04110", "Cell cycle", ResolutionStatus.RESOLVED)
    }

    @Test
    void "test Kegg database fetcher function for RESOLVED test of Kegg.orthology"() {
        testUtils.testFetcherWithAccession("K04459", "DUSP, MKP", ResolutionStatus.RESOLVED)
    }

    @Test
    void "test Kegg database fetcher function for RESOLVED test of Kegg.reaction"() {
        testUtils.testFetcherWithAccession("R01655", "5,10-Methenyltetrahydrofolate 5-hydrolase (decyclizing)", ResolutionStatus.RESOLVED)
    }

    @Test
    void "test Kegg database fetcher function for RESOLVED test of Kegg.genes"() {
        testUtils.testFetcherWithAccession("hsa:48", "ACO1, ACONS, HEL60, IREB1, IREBP, IREBP1, IRP1", ResolutionStatus.RESOLVED)
    }
}
