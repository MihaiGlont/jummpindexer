/**
 * Copyright (C) 2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/
package net.biomodels.jummp.indexing.referenceresolution

import net.biomodels.jummp.annotationstore.ResolutionStatus
import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.indexing.NonCachingUniProtFetcher
import net.biomodels.jummp.indexing.referenceresolution.dummyerrorclasses.DummyUniprotFetcherForURLConnection
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


/**
 * @author Chinmay Arankalle <carankalle@ebi.ac.uk>
 */
@RunWith(JUnit4.class)
class NonCachingUniProtFetcherSpec {
    TestUtils testUtils

    @Before
    void setUp() {
        testUtils = new TestUtils(new NonCachingUniProtFetcher())
    }

    @Test
    void "test UniProt fetcher function for RESOLVER_ERROR test"() {
        given: "ResourceReference object"
        def myXref = new ResourceReference(accession: "P24033", datatype: "uniprot", uri: "https://www.uniprot.org/uniprot/P24033.xml")
        def service = DummyUniprotFetcherForURLConnection.newInstance()
        testUtils.testFetcherWithDummyObjectForResolverError(myXref, service)
    }

    @Test
    void "test UniProt database fetcher function for RESOLVED test"() {
        testUtils.testFetcherWithAccessionAndDataType("P06652", "M-phase inducer phosphatase", ResolutionStatus.RESOLVED, "uniprot")
        testUtils.testFetcherWithAccessionAndDataType("3702", "Arabidopsis thaliana", ResolutionStatus.RESOLVED, "taxonomy")
        testUtils.testFetcherWithAccessionAndDataType("33090", "Viridiplantae", ResolutionStatus.RESOLVED, "taxonomy")
    }

    @Test
    void "test UniProt database fetcher function for UNRESOLVABLE test"() {
        testUtils.testFetcherWithAccessionAndDataType("C9JH64", null, ResolutionStatus.UNRESOLVABLE, "uniprot")
    }

    @Test
    void "test Taxonomy database fetcher function for RESOLVED test"() {
        testUtils.testFetcherWithAccessionAndDataType("4952", "Yarrowia lipolytica", ResolutionStatus.RESOLVED, "taxonomy")
    }
}
