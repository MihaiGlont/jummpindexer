/**
 * Copyright (C) 2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/
package net.biomodels.jummp.indexing.referenceresolution

import net.biomodels.jummp.annotationstore.ResolutionStatus
import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.indexing.NonCachingReactomeFetcher
import net.biomodels.jummp.indexing.referenceresolution.dummyerrorclasses.DummyReactomeFetcherForURLConnection
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


/**
 * @author Chinmay Arankalle <carankalle@ebi.ac.uk>
 */
@RunWith(JUnit4.class)
class NonCachingReactomeFetcherSpec {
    TestUtils testUtils

    @Before
    void setUp() {
        testUtils = new TestUtils(new NonCachingReactomeFetcher())
    }

    @Test
    void "test Reactome fetcher function for UNRESOLVABLE test"() {
        given: "ResourceReference object with R-HSA-179863"
        def myXref = new ResourceReference(accession: "R-HSA-179863", datatype: "reactome", uri: "https://www.reactome.org/ContentService/data/query/R-HSA-179863/displayName")
        def service = DummyReactomeFetcherForURLConnection.newInstance()
        testUtils.testFetcherWithDummyObjectForUnresolvable(myXref, service)
    }

    @Test
    void "test Reactome database fetcher function for UNRESOLVABLE test"() {
        testUtils.testFetcherWithAccession("REACT_2136", null, ResolutionStatus.UNRESOLVABLE)
    }

    @Test
    void "test Reactome database fetcher function for RESOLVED test"() {
        testUtils.testFetcherWithAccession("REACT_5246", "XIAP [cytosol]", ResolutionStatus.RESOLVED)
    }
}
