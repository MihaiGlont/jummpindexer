package net.biomodels.jummp.indexing

import net.biomodels.jummp.annotationstore.*
import net.biomodels.jummp.indexing.exporter.OmicsdiDataSetEntry
import net.biomodels.jummp.indexing.exporter.OmicsdiDataSetEntryBuilder
import net.biomodels.jummp.indexing.exporter.OmicsdiXmlBasedSchemaExporter
import net.biomodels.jummp.model.ModelElementType
import net.biomodels.jummp.model.Revision
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertTrue

/**
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 * @author Tung Nguyen <tung.nguyen@ebi.ac.uk>
 */
@RunWith(JUnit4.class)
class OmicsdiIndexerTest {
    private static final Logger logger = LoggerFactory.getLogger(OmicsdiIndexerTest.class)
    OmicsdiXmlBasedSchemaExporter generator

    @Before
    void setUp() {
        Qualifier curated = new Qualifier(uri: "curated")
        Statement isCuratedStatement = new Statement(subjectId: "#", qualifier: curated,
                object: new ResourceReference(uri: "true"))
        def isDescribedBy = new Qualifier(uri: "http://biomodels.net/model-qualifiers/isDescribedBy")
        ResourceReference pmid = new ResourceReference(uri: "http://identifiers.org/pubmed/12345678")
        def pmidStatement = new Statement(subjectId: "#", qualifier: isDescribedBy, object: pmid)
        String creatorId = 'somebody'
        ModelElementType modelElementType = new ModelElementType(name: "model")

        def defaultAnnotations = [
            new ElementAnnotation(modelElementType: modelElementType, creatorId: creatorId,
                    statement: isCuratedStatement),
            new ElementAnnotation(modelElementType: modelElementType, creatorId: creatorId,
                    statement: pmidStatement)
        ]
        RevisionAnnotation.metaClass.findAllByRevision = { Revision r ->
            defaultAnnotations
        }
    }

    /**
     * This test case aims to verify creating OmicsDI DataSet Entry.
     */
    @Test
    void testCreatingOmicsDIDataSetEntry() {
        String id = "0001"
        String name = "Model Sample"
        String description = "This is an model sample used for testing."
        Map<String, Date> dates = [:]
        def dateSubmitted = new Date()
        def datePublished = new Date()
        dates.put("submission", dateSubmitted)
        dates.put("publication", datePublished)
        OmicsdiDataSetEntry e = new OmicsdiDataSetEntryBuilder()
            .setFieldsInMandatorySection(id, name, description, dates)
            .build()
        assertEquals id, e.id
        assertEquals name, e.name
        assertEquals description, e.description
        assertEquals dateSubmitted, e.dates['submission']
        assertEquals datePublished, e.dates['publication']

        List<OmicsdiDataSetEntry> entries = new ArrayList<OmicsdiDataSetEntry>()
        entries.add(e)
        assertTrue(entries.size() > 0)
        generator = new OmicsdiXmlBasedSchemaExporter()
        def time = System.nanoTime()
        String res = generator.buildSchemaXmlAsString(entries)
        time = (System.nanoTime() - time) / 1000000.0
        logger.debug "It took ${time}ms to build xml string."
        assertTrue res.contains("<date type=\"publication\" value=\"${datePublished.format('yyyy-MM-dd')}\"")
        assertTrue res.contains("<date type=\"submission\" value=\"${dateSubmitted.format('yyyy-MM-dd')}\"")
        assertTrue res.contains("<entry id=\"$id\">")
        assertTrue res.contains("<name>$name</name>")
        assertTrue res.contains("<description>$description</description>")
	}
}
