package net.biomodels.jummp.indexing

import net.biomodels.jummp.annotationstore.ResourceReference
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Specification

class NonCachingKeggFetcherSpec extends Specification {
    private final static Logger logger = LoggerFactory.getLogger(NonCachingKeggFetcherSpec.class)

    def "try to fetch some terms"() {
        when: "using KeggFetcher processor to get term information"
        NonCachingKeggFetcher keggFetcher = new NonCachingKeggFetcher()
        def rr = new ResourceReference(accession: accession,
            collectionName: "KEGG Compound",
            datatype: "kegg.compound", uri: "http://identifiers.org/kegg.compound/${accession}")
        def res = keggFetcher.getTermInformation(rr)
        logger.info("The result is {}", res.dump())
        then: "the processor can get the right name"
        res.name == name
        where: "with different terms (i.e. accessions)"
        accession   || name
        "C12345"    || "1,3-Dimethyl-6,8-isoquinolinediol"  // kegg compound
        "G13032"    || "(Gal)3 (GlcNAc)2 (S)2"              // kegg glycan with composition considered as name
        "G00056"    || "III3,IV2Fuc-nLc4Cer"                // include both name and composition
        "ppa:PAS_chr2-1_0506" || "(RefSeq) Mitochondrial aldehyde dehydrogenase" // definition looks name
    }
}
