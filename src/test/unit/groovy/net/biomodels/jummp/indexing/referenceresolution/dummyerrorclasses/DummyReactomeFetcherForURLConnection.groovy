package net.biomodels.jummp.indexing.referenceresolution.dummyerrorclasses


import net.biomodels.jummp.indexing.NonCachingReactomeFetcher
import net.biomodels.jummp.indexing.referenceresolution.TestUtils

/**
 * @author carankalle on 03/10/2019.
 */
class DummyReactomeFetcherForURLConnection {
    static NonCachingReactomeFetcher newInstance() {
        new NonCachingReactomeFetcher(){
            @Override
            URLConnection openURLConnection(URL url)throws IOException{
                return TestUtils.openURLConnectionForFetcher(url)
            }
        }
    }
}
