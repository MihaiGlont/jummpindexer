package net.biomodels.jummp.indexing

import net.biomodels.jummp.core.model.ModelState
import net.biomodels.jummp.indexing.exporter.OmicsdiXmlBasedSchemaExporter
import net.biomodels.jummp.model.Model
import net.biomodels.jummp.model.Revision
import org.junit.experimental.categories.Category
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Specification

/**
 * @author  Tung Nguyen <tung.nguyen@ebi.ac.uk>
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 * @date    11/01/2018
 */
@Category(UnitTest.class)
class OmicsdiIndexerSpec extends Specification {
    private static final Logger logger = LoggerFactory.getLogger(OmicsdiIndexerSpec.class)

    /**
     * This test case aims to verify exporting OmicsDI DataSet Entry from a model without any
     * public revision.
     */
    def "model has no published revision"() {
        given: "a model, a revision and an OmicsDI generator"
        Model model = new Model()
        model.revisions = [] as Set
        Revision revision = new Revision()
        def generator = new OmicsdiXmlBasedSchemaExporter()

        when: "assign the revision to the model but do not publish the revision and select revisions for export"
        model.revisions.addAll([revision])
        ArrayList<Model> models = [model]
        ArrayList<Revision> result = generator.selectModelRevisionsForExport(models)

        then: "model is initialised properly and the generator produces an empty list"
        model
        result.size() == 1
    }

    /**
     * This test case aims to test whether selectModelRevisionsForExport method can be passed
     * the case that the published revision is not the latest one. The input is the model has
     * three revisions. The second revision is public and the others aren't.
     */
    def "model has a published revision which is not the latest one"() {
        setup: "a model has four revisions in which the published revision is the latest one"
        Model model = new Model()
        Revision revision1 = new Revision()
        Revision revision2 = new Revision()
        Revision revision3 = new Revision()
        revision1.setState(ModelState.UNPUBLISHED)
        revision2.setState(ModelState.PUBLISHED)
        revision3.setState(ModelState.UNPUBLISHED)
        Set revisions = new HashSet()
        revisions.add(revision1)
        revisions.add(revision2)
        revisions.add(revision3)
        def revision4 = new Revision()
        revision4.setState(ModelState.UNPUBLISHED)
        revisions.add(revision4)
        model.revisions = revisions
        logger.debug("Model: ${model}")
        logger.debug("Revisions: ${model.revisions.collect {it.state}}")

        when: "use an OmicsDI generator to extract the published revision to export"
        def models = [model]
        def omicsdiGenerator = new OmicsdiXmlBasedSchemaExporter()
        List<Revision> result =  omicsdiGenerator.selectModelRevisionsForExport(models)
        logger.info(result[0].toString())
        logger.debug(result.size().toString())

        then: "the generator can be aware of the right published revision"
        result
    }
}
