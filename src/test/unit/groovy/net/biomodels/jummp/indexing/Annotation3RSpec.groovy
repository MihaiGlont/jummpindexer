/**
 * Copyright (C) 2010-2018 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Specification

/**
 * Unit tests for AnnotationReferenceResolver.
 *
 * @author Tung Nguyen <tung.nguyen@ebi.ac.uk>
 */
class Annotation3RSpec extends Specification {
    private final static Logger logger = LoggerFactory.getLogger(Annotation3RSpec.class)

    def "Cross reference #a has collection #b and accession #c"() {
        given: "an AnnotationReferenceResolver object"
        AnnotationReferenceResolver aRR = new AnnotationReferenceResolver()

        when: "split data type and accession of CHEBI annotation url"
        String[] parts = aRR.instance().extractParts(a,
                AnnotationReferenceResolver.AnnoType.IDENTIFIER)

        then: "should have two values as data type (collection) and accession"
        logger.info(parts.toString())
        2 == parts.size()
        b == parts[0]
        c == parts[1]

        where:
        a << [
            "http://identifiers.org/chebi/CHEBI:28445",
            "http://identifiers.org/doi/10.1140/epjb/e2002-00271-1",
             "http://identifiers.org/inchi/InChI=1S/C2H6O/c1-2-3/h3H,2H2,1H3)"
        ]
        b << ["chebi", "doi", "inchi"]
        c << ["CHEBI:28445", "10.1140/epjb/e2002-00271-1", "InChI=1S/C2H6O/c1-2-3/h3H,2H2,1H3)"]
    }
}
