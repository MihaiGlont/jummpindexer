package net.biomodels.jummp.indexing.referenceresolution.dummyerrorclasses

import org.springframework.http.HttpStatus
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestClientException
import uk.ac.ebi.pride.utilities.ols.web.service.client.OLSClient
import uk.ac.ebi.pride.utilities.ols.web.service.model.Identifier
import uk.ac.ebi.pride.utilities.ols.web.service.model.Term
import uk.ac.ebi.pride.utilities.ols.web.service.config.AbstractOLSWsConfig

/**
 * @author carankalle on 02/10/2019.
 */
class DummyOLSClient {
    static OLSClient newInstance(AbstractOLSWsConfig config) {
        OLSClient olsClient = new OLSClient(config) {
            @Override
            Term getTermById(Identifier termId, String ontologyId) throws RestClientException {
                throw new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR)
            }
        }
        return olsClient
    }
}
