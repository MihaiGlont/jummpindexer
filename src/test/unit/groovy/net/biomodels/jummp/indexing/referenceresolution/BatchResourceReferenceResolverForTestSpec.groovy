/**
 * Copyright (C) 2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/
package net.biomodels.jummp.indexing.referenceresolution

import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.indexing.BatchResourceReferenceResolver
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

/**
 * @author Chinmay Arankalle <carankalle@ebi.ac.uk>
 */
@RunWith(JUnit4.class)
class BatchResourceReferenceResolverForTestSpec {
    @Before
    void setUp() {
        BatchResourceReferenceResolver.metaClass.updateReference = { ResourceReference xref ->
            xref.uri = 'http://identifiers.org/go/GO:33333'
        }
    }

    @Test
    void "test updateReference method"() {
        given: "ResourceReference object with obo.go type accession data and uri"
        def myXref = new ResourceReference(accession: "GO:33333", uri: "http://identifiers.org/obo.go/GO:33333")
        def service = new BatchResourceReferenceResolver()

        when:"when ResourceReference object is updated through updateReference"
        service.updateReference(myXref)

        then:"It should return modified uri"
        String expectedUri = "http://identifiers.org/go/GO:33333"
        assert expectedUri == myXref.getUri()
    }
}
