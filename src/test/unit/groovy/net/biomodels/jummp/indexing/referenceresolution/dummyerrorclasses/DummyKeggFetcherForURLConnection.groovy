package net.biomodels.jummp.indexing.referenceresolution.dummyerrorclasses


import net.biomodels.jummp.indexing.NonCachingKeggFetcher
import net.biomodels.jummp.indexing.referenceresolution.TestUtils

/**
 * @author carankalle on 03/10/2019.
 */
class DummyKeggFetcherForURLConnection {
    static NonCachingKeggFetcher newInstance() {
        new NonCachingKeggFetcher(){
            @Override
            URLConnection openURLConnection(URL url)throws IOException{
                return TestUtils.openURLConnectionForFetcher(url)
            }
        }
    }
}
