/**
 * Copyright (C) 2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/
package net.biomodels.jummp.indexing.referenceresolution

import net.biomodels.jummp.annotationstore.ResolutionStatus
import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.indexing.NonCachingOLSFetcher
import net.biomodels.jummp.indexing.referenceresolution.dummyerrorclasses.DummyOLSClient
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

/**
 * @author Chinmay Arankalle <carankalle@ebi.ac.uk>
 */
@RunWith(JUnit4.class)
class NonCachingOLSFetcherSpec {
    TestUtils testUtils
    @Before
    void setUp() {
        testUtils = new TestUtils(new NonCachingOLSFetcher())
    }
    @Test
    void "test OLS fetcher function for RESOLVER_ERROR test for CHEBI"() {
        given: "ResourceReference object with CHEBI:18059 and setting injecting OLSClient object"
        def myXref = new ResourceReference(accession: "CHEBI:18059",datatype:"chebi", uri: "http://identifiers.org/obo.go/GO:33333")
        def service = new NonCachingOLSFetcher()
        service.setOlsClient(DummyOLSClient.newInstance(service.olsClient.getConfig()))

        when:"when ResourceReference object is updated through getTermInformation"
        service.getTermInformation(myXref)
        then:"It should modify ResolutionStatus of the ResourceReference object"
        assert myXref.getResolutionStatus() == ResolutionStatus.UNRESOLVABLE
    }
    @Test
    void "test OLS fetcher function for RESOLVED test for CHEBI"() {
        testUtils.testFetcherWithAccessionAndDataType("CHEBI:18059", "lipid", ResolutionStatus.RESOLVED, "chebi")
    }


    @Test
    void "test  OLS fetcher function for RESOLVED test of GO"() {
        testUtils.testFetcherWithAccessionAndDataType("GO:0042588", "zymogen granule", ResolutionStatus.RESOLVED, "go")
    }


    @Test
    void "test OLS fetcher fetcher function for UNRESOLVABLE test of PR"() {
        testUtils.testFetcherWithAccessionAndDataType("PR:000000547", "tumor necrosis factor ligand superfamily member 6 isoform FasL soluble form", ResolutionStatus.RESOLVED, "pr")
    }

    @Test
    void "test OLS fetcher fetcher function for UNRESOLVABLE test of SBO"() {
        testUtils.testFetcherWithAccessionAndDataType("SBO:0000016", "unimolecular rate constant", ResolutionStatus.RESOLVED, "sbo")
    }

    @Test
    void "test OLS fetcher fetcher function for UNRESOLVABLE test of PW"() {
        testUtils.testFetcherWithAccessionAndDataType("PW:0002596", "direct factors Xa inhibitor drug pathway", ResolutionStatus.RESOLVED, "pw")
    }

    @Test
    void "test OLS fetcher fetcher function for UNRESOLVABLE test of CL"() {
        testUtils.testFetcherWithAccessionAndDataType("CL:0000125", "glial cell", ResolutionStatus.RESOLVED, "cl")
    }

    @Test
    void "test OLS fetcher fetcher function for UNRESOLVABLE test of BTO"() {
        testUtils.testFetcherWithAccessionAndDataType("BTO:0001044", "phagocyte", ResolutionStatus.RESOLVED, "bto")
    }

    @Test
    void "test OLS fetcher fetcher function for RESOLVED test of efo"() {
        testUtils.testFetcherWithAccessionAndDataType("1000307", "Invasive Breast Carcinoma", ResolutionStatus.RESOLVED, "efo")
    }

    @Test
    void "test OLS fetcher fetcher function for RESOLVED test of NCIT"() {
        testUtils.testFetcherWithAccessionAndDataType("C25201", "Sensitivity", ResolutionStatus.RESOLVED, "ncit")
    }

    @Test
    void "test OLS fetcher fetcher function for RESOLVED test of obo.go"() {
        testUtils.testFetcherWithAccessionAndDataType("GO:0008286", "insulin receptor signaling pathway", ResolutionStatus.RESOLVED, "go")
    }

    @Test
    void "test OLS fetcher function for RESOLVED test of biomodels.sbo"() {
        testUtils.testFetcherWithAccessionAndDataType("SBO:0000316", "microRNA", ResolutionStatus.RESOLVED, "sbo")
    }

    @Test
    void "test OLS fetcher function for UNRESOLVABLE test for CHEBI"() {
        testUtils.testFetcherWithAccessionAndDataType("CHEBI:37522", null, ResolutionStatus.UNRESOLVABLE, "chebi")
    }
}
