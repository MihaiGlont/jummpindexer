/**
 * Copyright (C) 2010-2017 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing.miriam

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

import static org.junit.Assert.*

/**
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 */
@RunWith(JUnit4.class)
class MiriamRegistryServiceTest {
    @Test
    void nullURIsAreRejected() {
        String uri = null
        assertNull MiriamRegistryService.updateURI(uri)
        uri = ""
        assertEquals(uri, MiriamRegistryService.updateURI(uri))
        uri = "    "
        assertEquals(uri, MiriamRegistryService.updateURI(uri))
    }

    @Test
    void deprecatedURIsAreUpdated() {
        assertNull MiriamRegistryService.updateURI("http://identifiers.org/foo/bar:123")
        String btoUri = "http://identifiers.org/obo.bto/BTO:0000316"
        String sboUri = "http://identifiers.org/biomodels.sbo/SBO:0000177"

        String expectedBTOUri = "http://identifiers.org/bto/BTO:0000316"
        String expectedSBOUri = "http://identifiers.org/sbo/SBO:0000177"

        assertEquals(expectedBTOUri, MiriamRegistryService.updateURI(btoUri))
        assertEquals(expectedSBOUri, MiriamRegistryService.updateURI(sboUri))
        String expected = "http://identifiers.org/chebi/CHEBI:28445"
        assertEquals(expected, MiriamRegistryService.updateURI(expected))
    }

    @Test
    void collectionNamesAreResolved() {
        [
            "" : "",
            (null) : null,
            "this-does-not-exist" : "",
            "http://identifiers.org/chebi/" : "ChEBI",
            "http://identifiers.org/taxonomy" : "Taxonomy",
            "http://identifiers.org/obo.bto/" : "Brenda Tissue Ontology",
            "http://identifiers.org/obo.bto" : "Brenda Tissue Ontology",
            "obo.bto" : "",
            "bto" : "",
            "http://identifiers.org/obo.bto/BTO:0000123" : "",
            "http://www.ebi.ac.uk/biomodels" : "BioModels Database",
            "http://identifiers.org/biomodels.db" : "BioModels Database",
            "http://identifiers.org/obo.go" : "Gene Ontology",
            "http://identifiers.org/sbo" : "Systems Biology Ontology",
        ].each { uri, name ->
            assertEquals name, MiriamRegistryService.fetchCollectionName(uri)
        }
    }
}
