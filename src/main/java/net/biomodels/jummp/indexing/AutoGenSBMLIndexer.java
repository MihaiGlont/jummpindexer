/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing;

import org.sbml.jsbml.CVTerm;
import org.sbml.jsbml.Model;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Indexer for automatically-generated SBML models.
 *
 * Unline {@link StreamingSBMLIndexer}, it only persists model-level annotations in the database.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tung.nguyen@ebi.ac.uk>
 */
public class AutoGenSBMLIndexer extends StreamingSBMLIndexer {

    @Override
    protected void persistElementAnnotations(Model model, ModelElementTypeResolver elementTypeResolver, long revisionId,
            String authorName) {
        Stream<CVTerm> cvTerms = SbmlUtils.streamModelLevelCVTerms(model);
        doPersistElementAnnotations(cvTerms, elementTypeResolver, revisionId, authorName);
    }

    @Override
    protected void persistResourceReferences(Model model) {
        Set<String> cvTerms = SbmlUtils.getModelLevelAnnotations(model);
        doPersistResourceReferences(cvTerms);
    }

    @Override
    protected void persistQualifiers(Model model) {
        Set<String> qualifiers = SbmlUtils.streamModelLevelCVTerms(model)
            .map(SbmlUtils::getUriForQualifier)
            .collect(Collectors.toSet());

        doPersistQualifiers(qualifiers);
    }
}
