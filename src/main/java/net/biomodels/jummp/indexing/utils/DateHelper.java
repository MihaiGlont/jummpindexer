package net.biomodels.jummp.indexing.utils;

public class DateHelper {
    public static String formatDuration(long duration) {
        return Double.toString(((double) duration) / Math.pow(10, 9)) + " s";
    }
}
