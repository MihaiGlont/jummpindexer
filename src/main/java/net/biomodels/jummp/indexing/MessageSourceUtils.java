/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing;

import org.springframework.context.MessageSource;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

public class MessageSourceUtils {
    private static volatile MessageSource messageSource = null;

    public static String convertErrorsToString(Errors instanceErrors) {
        final List<ObjectError> allErrors = Objects.requireNonNull(instanceErrors).getAllErrors();
        if (allErrors.isEmpty()) return "";

        return allErrors.stream()
            .map(MessageSourceUtils::convertObjectErrorToString)
            .collect(Collectors.joining(";"));
    }

    private static String convertObjectErrorToString(ObjectError e) {
        return getMessageSource()
            .getMessage(e.getCode(), e.getArguments(), e.getDefaultMessage(), Locale.getDefault());
    }

    private static MessageSource getMessageSource() {
        if (null == messageSource) {
            messageSource = GormUtil.getAppCtx().getBean("messageSource", MessageSource.class);
        }
        return messageSource;
    }
}
