/*
 * Copyright (C) 2010-2020 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author raza
 */
@EnableTransactionManagement
public class Indexer {
    private static final Logger log = LoggerFactory.getLogger(Indexer.class);
    private static final boolean IS_DEBUG_ENABLED = log.isDebugEnabled();

    public static void main(String[] args) {
        if (args.length < 1) {
            log.error("Please pass the model's indexing context as an argument.");
            return;
        }
        String ctxPath = args[0];
        String option = args.length == 2 ? args[1] : null;
        if (null != option) {
            ExportType exportType = getExportType(option);

            RequestParser.generateMetadataForSearchServer(ctxPath, exportType);
            log.debug("End up generating the files.");
        } else {
            // case 2: run indexing process
            if (IS_DEBUG_ENABLED) {
                log.debug("Begin indexing process for " + ctxPath);
            }
            RequestParser.handleRequest(ctxPath);
            if (IS_DEBUG_ENABLED) {
                log.debug("End up indexing process for " + ctxPath);
            }
        }
    }

    private static ExportType getExportType(String option) {
        ExportType defaultExportType = ExportType.OMICS_DI;
        ExportType exportType = ExportType.forOption(option);
        if (null == exportType) {
            String msg = String.format("Ignoring unexpected export type '%s'. Proceeding with the %s export",
                option, defaultExportType.name());
            log.warn(msg);
            exportType = defaultExportType;
        }
        return exportType;
    }
}
