package net.biomodels.jummp.indexing;

import net.biomodels.jummp.indexing.utils.DateHelper;
import org.sbml.jsbml.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.tree.TreeNode;
import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class SbmlUtils {
    private static final Logger logger = LoggerFactory.getLogger(SbmlUtils.class);

    static SBase getSBaseForCVTerm(CVTerm term) {
        return (SBase) term.getParent().getParent();
    }

    static Set<String> getModelLevelAnnotations(Model model) {
        Stream<CVTerm> cvTerms = streamModelLevelCVTerms(model);
        return getCVTermAnnotations(cvTerms);
    }

    public static Set<String> getAllModelAnnotations(Model model) {
        Stream<CVTerm> cvTerms = convertModelToFlattenedCVTermStream(model);
        return getCVTermAnnotations(cvTerms);
    }

    public static Map<String, Set<String>> getAllAnnotationsWithQualifiers(Model model) {
        Stream<CVTerm> cvTerms = convertModelToFlattenedCVTermStream(model);
        return getCVTermAnnotationMap(cvTerms);
    }

    static Stream<CVTerm> streamModelLevelCVTerms(Model model) {
        List<CVTerm> cvTerms = model.getCVTerms();
        return cvTerms.stream().flatMap(SbmlUtils::flattenCVTerm);
    }

    static Stream<CVTerm> convertModelToFlattenedCVTermStream(Model model) {
        return convertModelToStream(model)
            .flatMap(s -> s.getCVTerms().stream())
            .flatMap(SbmlUtils::flattenCVTerm);
    }

    static Stream<? extends SBase> convertModelToStream(Model model) {
        Stream<? extends  SBase> nodes = visit(model);
        return nodes;
    }


    static String getUriForQualifier(CVTerm term) {
        Objects.requireNonNull(term);
        String ns = term.getQualifierType().getNamespaceURI();
        String accession = term.getQualifier().getElementNameEquivalent();
        if (null == ns) {
            ns = term.isBiologicalQualifier() ? CVTerm.URI_BIOMODELS_NET_BIOLOGY_QUALIFIERS :
                CVTerm.URI_BIOMODELS_NET_MODEL_QUALIFIERS;
        }
        return ns + accession;
    }

    public static Model parseSbmlModel(String path) throws XMLStreamException, FileNotFoundException {
        SBMLDocument d = null;
        Model model;

        long now = System.nanoTime();
        try (InputStream in = new BufferedInputStream(new FileInputStream(path))) {
            SBMLReader r = new SBMLReader();
            d = r.readSBMLFromStream(in);
            long parseTime = System.nanoTime() - now;
            logger.info("JSBML parseTime: {}", DateHelper.formatDuration(parseTime));
        } catch (IOException e) {
            logger.error("I/O exception while reading " + path, e);
        }
        if (null == d) {
            return null;
        }
        model = d.getModel();
        return model;
    }

    private static Map<String, Set<String>> getCVTermAnnotationMap(Stream<CVTerm> cvTerm) {
        Map<String, Set<String>> map = new LinkedHashMap<>();
        Set<CVTerm> terms = cvTerm.collect(Collectors.toSet());
        for (CVTerm term : terms) {
            List<String> resources = term.getResources();
            Set<String> resourceSet = new HashSet<>(resources);
            String qualifier = term.getQualifier().getElementNameEquivalent();
            if (map.containsKey(qualifier)) {
                map.get(qualifier).addAll(resourceSet);
            } else {
                map.put(qualifier, resourceSet);
            }
        }
        return map;
    }

    private static Set<String> getCVTermAnnotations(Stream<CVTerm> cvTerms) {
        return cvTerms
            .flatMap(t -> t.getResources().stream())
            .map(String::trim)
            .collect(Collectors.toSet());
    }

    private static <S extends SBase> Stream<S> visit(TreeNode n) {
        Objects.requireNonNull(n, "Refusing to process an undefined element");
        boolean isSBase = n instanceof SBase;
        if (!isSBase) {
            return Stream.empty();
        }

        Stream<S> childStream = StreamSupport.stream(
            new CompositeSBaseSpliterator(n), false)
            .flatMap(SbmlUtils::<S>visit);
        if (n instanceof ListOf) {
            return childStream;
        }
        // noinspection unchecked -- we have already checked n is a kind of SBase
        return Stream.concat(
            Stream.of((S) n),
            childStream
        );
    }

    private static Stream<CVTerm> flattenCVTerm(CVTerm term) {
        Objects.requireNonNull(term);
        Stream<CVTerm> selfStream = Stream.of(term);
        if (term.getChildCount() == 0) {
            return selfStream;
        }
        Stream<CVTerm> terms = Stream.concat(selfStream,
            term.getListOfNestedCVTerms().stream().flatMap(SbmlUtils::<CVTerm>flattenCVTerm));

        return terms;
    }
}
