/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing;

import net.biomodels.jummp.annotationstore.*;
import net.biomodels.jummp.indexing.miriam.MiriamRegistryService;
import net.biomodels.jummp.indexing.utils.DateHelper;
import net.biomodels.jummp.model.ModelElementType;
import net.biomodels.jummp.model.ModelFormat;
import net.biomodels.jummp.model.Revision;
import net.biomodels.jummp.plugins.security.Person;
import net.biomodels.jummp.plugins.security.User;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.stat.SessionStatistics;
import org.sbml.jsbml.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.swing.tree.TreeNode;
import javax.validation.constraints.NotNull;
import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * SBML indexer which uses Java8 streams to process entities
 * and multiple threads to persist the annotations that get generated.
 *
 * demonstrates functionality by indexing the largest model in BioModels.
 */
public class StreamingSBMLIndexer {
    private static final String REVISION_NAME =
        "Smallbone2013 - Human metabolism global reconstruction (recon 2.1x)";
    protected final ExecutorService pool;
    private static final String AUTHOR_NAME = "not me";
    private static long now = System.nanoTime();
    protected TxSupport transactionSupport;
    protected RequestContext requestContext;
    protected long revisionId;
    protected ModelFormat format;
    protected ModelElementTypeResolver elementTypeResolver;

    private static final Logger log = LoggerFactory.getLogger(StreamingSBMLIndexer.class);

    public StreamingSBMLIndexer() {
        transactionSupport = new TxSupport();
        final int hardwareThreadCount = Runtime.getRuntime().availableProcessors() * 2;
        pool = new DatastoreWorkerPool(Math.min(GormUtil.MAX_POOL_SIZE / 2, hardwareThreadCount));
    }

    public static void main(String[] args) throws InterruptedException {
        StreamingSBMLIndexer self = new StreamingSBMLIndexer();

        try {
            // uncomment if manually attaching a profiler
            // Thread.sleep(java.util.concurrent.TimeUnit.SECONDS.toMillis(30L));
            // basic initialisation -- build a request context and add seed data if needed
            self.init();
            log.info("begin...");
            String path = "src/test/resources/models/sbml-anno/MODEL1311110001.xml"; /* recon 2.1 */
            Model m = SbmlUtils.parseSbmlModel(path);
            self.persist(m, self.elementTypeResolver, self.revisionId, AUTHOR_NAME);
        } catch (XMLStreamException | FileNotFoundException ignored) {
            throw new IllegalStateException("The model is not a valid XML file.");
        } catch (Exception e) {
            log.error("OOPS: ", e);
        } finally {
            now = System.nanoTime() - now;
            int statements = BatchAnnotationStatementInsertionWorker.insertedStatements.get();
            String duration = DateHelper.formatDuration(now);
            log.info("Persisted {} statements in {}.", statements, duration);
            GormUtil.closeSession();
            log.info("end.");
        }
    }

    protected void closeDatastoreWorkers() {
        pool.shutdown(); //reject further submissions
        try {
            if (!pool.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES)) {
                List<Runnable> interruptedWorkers = pool.shutdownNow();
                if (!interruptedWorkers.isEmpty()) {
                    log.error("{} worker threads did not finish in time.",
                            interruptedWorkers.size());
                }
                while (!pool.awaitTermination(10L, TimeUnit.SECONDS)) {
                    log.info("Waiting for Hibernate workers...");
                }
            }
        } catch (InterruptedException e) {
            log.error("interrupted while shutting down the thread pool.");
            pool.shutdownNow();
            Thread.currentThread().interrupt();
        }

    }

    /* first persists cross-references and qualifiers, then saves the statements and element annotations */
    /* this is to avoid situations where two threads attempt to insert the same cross-reference / qualifier */
    /* it may be possible to merge these three processes into one */
    public void persist(Model model, ModelElementTypeResolver elementTypeResolver,
            long revisionId, String authorName) {
        persistResourceReferences(model);
        long delta = System.nanoTime() - now;
        log.info("persist xrefs = {}", DateHelper.formatDuration(delta));
        now = System.nanoTime();
        persistQualifiers(model);
        delta = System.nanoTime() - now;
        log.info("persist qualifiers = {}", DateHelper.formatDuration(delta));
        now = System.nanoTime();
        persistElementAnnotations(model, elementTypeResolver, revisionId, authorName);
        delta = System.nanoTime() - now;
        log.info("persist EAs = {}", DateHelper.formatDuration(delta));
    }

    protected void persistElementAnnotations(Model model, ModelElementTypeResolver
            elementTypeResolver, long revisionId, String authorName) {
        // build a stream of CVTerms
        Stream<CVTerm> cvTermStream = SbmlUtils.convertModelToFlattenedCVTermStream(model);
        doPersistElementAnnotations(cvTermStream, elementTypeResolver, revisionId, authorName);
    }

    protected void doPersistElementAnnotations(Stream<CVTerm> cvTermStream, ModelElementTypeResolver elementTypeResolver,
            long revisionId, String authorName) {
        List<CVTerm> cvTerms = FixedBatchSpliterator.withBatchSize(cvTermStream, 100)
                .collect(Collectors.toList());
        // Partition it into chunks of 1000 entries each
        List<List<CVTerm>> cvTermPartitions = ListPartitionSupport.partitionUsingStreams(
            cvTerms, 100);
        // process each chunk concurrently
        try {
            cvTermPartitions.forEach(bag ->
                pool.submit(new BatchAnnotationStatementInsertionWorker(bag, GormUtil.BATCH_SIZE,
                    elementTypeResolver, revisionId, authorName)));
        } finally {
            closeDatastoreWorkers();
        }
    }

    private void init() {
        // build a minimal context so that we can open a session before doing any database work
        requestContext = buildDummyRequestContext();
        GormUtil.initGorm(requestContext);
        GormUtil.openSession();
        transactionSupport = new TxSupport();

        insertSeedData();

        populateSbmlCoreModelElementTypes(format);
        elementTypeResolver = new ModelElementTypeResolver(format);
    }

    @SuppressWarnings("unchecked")
    private void insertSeedData() {
        final String modelFormatId = "SBML";
        final String modelFormatVersion = "L2V4";
        format = findModelFormatByIdAndVersion(modelFormatId, modelFormatVersion);
        requestContext.setModelFormat(modelFormatId);
        @SuppressWarnings("unchecked") Map<String, Object> contextData =
            requestContext.getPartialData();
        contextData.put("modelFormat", modelFormatId);
        contextData.put("levelVersion", modelFormatVersion);

        Revision revision = Revision.findWhere(Collections.singletonMap("name", REVISION_NAME));
        if (null == revision) {
            revisionId = transactionSupport.withNamedTransactionTemplate(
                TxSupport.makeNamedTxDefinition("insert seed data"),
                status -> doInsertRevisionData());
            revision = Revision.get(revisionId);
        } else {
            revisionId = revision.getId();
        }

        contextData.put("model_id", revision.getModel().getId());
        contextData.put("revision_id", revisionId);
    }

    private long doInsertRevisionData() {
        Map<String, Object> flushSaveParam = new LinkedHashMap<>(2, 1.0f);
        flushSaveParam.put("flush", "true");
        flushSaveParam.put("failOnError","true");

        Person p = new Person();
        p.setUserRealName(AUTHOR_NAME);
        p.setInstitution("M.E. PLC");

        p.save(flushSaveParam);

        User self = new User();
        self.setUsername("me");
        self.setEmail("my@self.name");
        self.setPerson(p);
        self.setPasswordExpired(false);
        self.setPassword("obscure");
        self.setAccountExpired(false);
        self.setAccountLocked(false);
        self.setEnabled(true);

        self.save(flushSaveParam);

        net.biomodels.jummp.model.Model model = new net.biomodels.jummp.model.Model();
        model.setVcsIdentifier("test/");
        model.setSubmissionId("MODEL1311110001");
        Revision revision = new Revision();
        revision.setModel(model);
        revision.setVcsId("123abc");
        revision.setRevisionNumber(1);
        revision.setOwner(self);
        revision.setMinorRevision(false);
        revision.setName(REVISION_NAME);
        revision.setUploadDate(new Date());
        revision.setFormat(format);
        revision.setDescription("Recon 2 is a highly curated reconstruction of the human " +
            "metabolic network. Whilst the network is state of the art, it has shortcomings," +
            " including the presence of unbalanced reactions involving generic metabolites. By " +
            "replacing these generic molecules with each of their specific instances, we can " +
            "ensure full elemental balancing, in turn allowing constraint-based analyses to be " +
            "performed. The resultant model, called Recon 2.1, is an order of magnitude larger " +
            "than the original.");

        model.setRevisions(Collections.singleton(revision));

        model.save(flushSaveParam);
        revision.save(flushSaveParam);

        log.info("model = {}", model);
        return revision.getId();
    }

    private static RequestContext buildDummyRequestContext() {
        RequestContext ctx = new RequestContext();
        String preferredLocation = System.getenv("JUMMP_CONFIG");
        Path preferredPath;
        if (null == preferredLocation) {
            String home = System.getProperty("user.home");
            Path defaultPath = FileSystems.getDefault().getPath(home, ".jummp.properties");
            if (!defaultPath.toFile().exists()) {
                throw new IllegalStateException("jummp properties file missing");
            }
            preferredPath = defaultPath;
        } else {
            preferredPath = FileSystems.getDefault().getPath(preferredLocation);
        }
        ctx.setConfigFilePath(preferredPath.toString());
        ctx.setPartialData(new LinkedHashMap<String, Object>());
        return ctx;
    }

    private static void populateSbmlCoreModelElementTypes(ModelFormat format) {
        String[] entityNames = {"model", "compartment", "constraint", "event", "functionDefinition",
            "initialAssignment", "parameter", "reaction", "rule", "species"};

        for (String name : entityNames) {
            Map<String, Object> properties = new HashMap<>(3, 1.0f);
            properties.put("name", name);
            properties.put("modelFormat", format);
            ModelElementType.findOrSaveWhere(properties);
        }
    }

    private static ModelFormat findModelFormatByIdAndVersion(String id, String version) {
        Map<String, Object> properties = new HashMap<>(5, 1.0f);
        properties.put("formatVersion", version);
        properties.put("identifier", id);
        ModelFormat format = ModelFormat.findOrSaveWhere(properties);
        if (null == format) {
            throw new IllegalStateException(String.format("Cannot save model format %s %s.",
                id, version));
        }
        return format;
    }

    protected void persistResourceReferences(Model model) {
        Set<String> annotations = SbmlUtils.getAllModelAnnotations(model);
        doPersistResourceReferences(annotations);
    }

    protected void doPersistResourceReferences(Set<String> annotations) {
        Set<InsertResourceReferenceQuery> queries = FixedBatchSpliterator.withBatchSize(
            annotations.stream(), 512)
            .filter(uri -> null == transactionSupport.withNamedTransactionTemplate(
                TxSupport.makeNamedReadOnlyTxDefinition("lookup for xref " + uri),
                new FindResourceReferenceByUri(uri)))
            .map(InsertResourceReferenceQuery::new).collect(Collectors.toSet());
        // control size of first-level cache
        // https://docs.jboss.org/hibernate/orm/4.3/manual/en-US/html/ch15.html#batch-inserts
        final int[] count = {0};
        final Session session = GormUtil.getSessionFactory().getCurrentSession();
        FlushMode oldFlushMode = session.getFlushMode();
        try {
            session.setFlushMode(FlushMode.MANUAL);
            transactionSupport.<Void>withNamedTransactionTemplate(
                TxSupport.makeNamedTxDefinition("xref insertion"), (TransactionStatus status) -> {
                    for (InsertResourceReferenceQuery q : queries) {
                        q.apply(status);
                        if (++count[0] % GormUtil.BATCH_SIZE == 0) {
                            status.flush();
                            session.clear();
                        }
                    }
                    return null;
                });
            session.flush();
        } finally {
            session.setFlushMode(oldFlushMode);
        }
    }

    protected void persistQualifiers(Model model) {
        Set<String> uriSet = SbmlUtils.convertModelToFlattenedCVTermStream(model)
            .map(SbmlUtils::getUriForQualifier)
            .collect(Collectors.toSet());

        doPersistQualifiers(uriSet);
    }

    protected void doPersistQualifiers(Set<String> uriSet) {
        Session session = GormUtil.getSessionFactory().getCurrentSession();
        FlushMode oldFlushMode = session.getFlushMode();
        try {
            session.setFlushMode(FlushMode.MANUAL);
            uriSet
                .stream()
                .filter((String uri) -> null == transactionSupport.withNamedTransactionTemplate(
                    TxSupport.makeNamedReadOnlyTxDefinition("lookup for qualifier " + uri),
                    new FindQualifierId(uri)))
                .forEach(uri -> transactionSupport.withNamedTransactionTemplate(
                    TxSupport.makeNamedTxDefinition("insertion of qualifier " + uri),
                    new InsertQualifierQuery(uri)));
            session.flush();
        } finally {
            session.setFlushMode(oldFlushMode);
        }
    }
}

/* fixed-size spliterators are used to partition streams into batches */
// https://www.airpair.com/java/posts/parallel-processing-of-io-based-data-with-java-streams
@SuppressWarnings("WeakerAccess")
abstract class FixedBatchSpliteratorBase<T> implements Spliterator<T> {
    private final int batchSize;
    private final int characteristics;
    private long est;

    public FixedBatchSpliteratorBase(int characteristics, int batchSize, long est) {
        this.characteristics = characteristics | SUBSIZED;
        this.batchSize = batchSize;
        this.est = est;
    }

    public FixedBatchSpliteratorBase(int characteristics, int batchSize) {
        this(characteristics, batchSize, Long.MAX_VALUE);
    }

    public FixedBatchSpliteratorBase(int characteristics) {
        this(characteristics, 128, Long.MAX_VALUE);
    }

    @SuppressWarnings("unused")
    public FixedBatchSpliteratorBase() {
        this(IMMUTABLE | ORDERED | NONNULL);
    }

    @Override
    public Spliterator<T> trySplit() {
        final HoldingConsumer<T> holder = new HoldingConsumer<>();
        if (!tryAdvance(holder)) return null;
        final Object[] a = new Object[batchSize];
        int j = 0;
        do a[j] = holder.value; while (++j < batchSize && tryAdvance(holder));
        if (est != Long.MAX_VALUE) est -= j;
        return Spliterators.spliterator(a, 0, j, characteristics() | SIZED);
    }

    @Override
    public Comparator<? super T> getComparator() {
        if (hasCharacteristics(SORTED)) return null;
        throw new IllegalStateException();
    }

    @Override
    public long estimateSize() {
        return est;
    }

    @Override
    public int characteristics() {
        return characteristics;
    }

    static final class HoldingConsumer<T> implements Consumer<T> {
        Object value;

        @Override
        public void accept(T value) {
            this.value = value;
        }
    }
}

@SuppressWarnings("WeakerAccess")
class FixedBatchSpliterator<T> extends FixedBatchSpliteratorBase<T> {
    private final Spliterator<T> spliterator;

    public FixedBatchSpliterator(Spliterator<T> toWrap, int batchSize) {
        super(toWrap.characteristics(), batchSize, toWrap.estimateSize());
        this.spliterator = toWrap;
    }

    public static <T> FixedBatchSpliterator<T> batchedSpliterator(Spliterator<T> toWrap,
        int batchSize) {
        return new FixedBatchSpliterator<>(toWrap, batchSize);
    }

    @SuppressWarnings("SameParameterValue")
    public static <T> Stream<T> withBatchSize(Stream<T> in, int batchSize) {
        return StreamSupport.stream(batchedSpliterator(in.spliterator(), batchSize), false);
    }

    @Override
    public boolean tryAdvance(Consumer<? super T> action) {
        return spliterator.tryAdvance(action);
    }

    @Override
    public void forEachRemaining(Consumer<? super T> action) {
        spliterator.forEachRemaining(action);
    }
}

/* we rely on JSBML's TreeNode support to recursively a stream of SBase entities */
final class CompositeSBaseSpliterator extends FixedBatchSpliteratorBase<SBase> {
    private static final int FLAGS = Spliterator.ORDERED | Spliterator.IMMUTABLE | Spliterator.NONNULL;
    private static final int DEFAULT_BATCH_SIZE = 10;
    private final Enumeration<TreeNode> collection;

    @SuppressWarnings("unused")
    CompositeSBaseSpliterator(TreeNode parent) {
        this(parent, DEFAULT_BATCH_SIZE);
    }

    @SuppressWarnings({"unchecked", "WeakerAccess"})
    CompositeSBaseSpliterator(TreeNode parent, int batchSize) {
        super(FLAGS, batchSize);
        this.collection = parent.children();
    }

    @Override
    public boolean tryAdvance(Consumer<? super SBase> action) {
        if (!collection.hasMoreElements()) return false;

        TreeNode next = collection.nextElement();
        if (!(next instanceof SBase)) {
            return tryAdvance(action);
        }
        action.accept((SBase) next);
        return true;
    }
}

/* a worker thread that processes a batch of CVTerms and persists the associated annotations */
class BatchAnnotationStatementInsertionWorker implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(
            BatchAnnotationStatementInsertionWorker.class);

    private final List<CVTerm> TO_CONSUME;
    private final int FLUSH_SIZE;
    static AtomicInteger insertedStatements = new AtomicInteger();
    private final String author;
    private final long revisionId;
    private final ModelElementTypeResolver typeResolver;

    BatchAnnotationStatementInsertionWorker(List<CVTerm> toConsume, int flushSize,
        ModelElementTypeResolver elementTypeResolver, long revisionId, String authorName) {
        TO_CONSUME = toConsume;
        FLUSH_SIZE = flushSize;
        this.typeResolver = elementTypeResolver;
        this.revisionId = revisionId;
        this.author = authorName;
    }

    @Override
    public void run() {
        SessionFactory sessionFactory = GormUtil.getSessionFactory();
        Session session = sessionFactory.getCurrentSession();
        TxSupport transactionManager = new TxSupport();

        for (int i = 0; i < TO_CONSUME.size(); ++i) {
            Set<ElementAnnotation> statements = null;
            CVTerm term;
            try {
                term = TO_CONSUME.get(i);
                statements = transactionManager.withNamedTransactionTemplate(
                    TxSupport.makeNamedTxDefinition("Statement insertion for term " + term),
                    new CVTermToAnnotationStatementConverter(term, typeResolver, revisionId, author)
                );
                final int stmtCount = statements.size();
                insertedStatements.addAndGet(stmtCount);
                if (i % FLUSH_SIZE == 0) {
                    session.flush();
                    session.clear();
                }
            } catch (NamedTransactionRollbackException dbException) {
                log.error(dbException.getMessage(), dbException);
                if (null != statements) {
                    log.error("Evicted {} statements.", statements.size());
                    statements.forEach(session::evict);
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }
}

/* customised thread pool executor which starts a hibernate session before executing a worker */
class DatastoreWorkerPool extends ThreadPoolExecutor {
    private final static String THREAD_NAME_PREFIX = "datastoreWorker";
    private static final Logger log = LoggerFactory.getLogger(DatastoreWorkerPool.class);


    @SuppressWarnings("unused")
    DatastoreWorkerPool() {
        super(0, Integer.MAX_VALUE, 30L, TimeUnit.SECONDS, new SynchronousQueue<>(),
            new CustomizableThreadFactory(THREAD_NAME_PREFIX));
    }

    DatastoreWorkerPool(int size) {
        super(size, size, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(),
            new CustomizableThreadFactory(THREAD_NAME_PREFIX));
    }

    @Override
    protected void beforeExecute(Thread t, Runnable r) {
        if (!GormUtil.isSessionAlreadyBound()) {
            GormUtil.openSession();
        } else {
            final SessionStatistics stats = GormUtil.getSessionFactory().getCurrentSession()
                    .getStatistics();
            log.warn("Thread {} already has an open Hibernate session with {} entities and {} collections. May clash with {}",
                    t.getName(), stats.getEntityCount(), stats.getCollectionCount(), r.toString());
        }
        super.beforeExecute(t, r);
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        super.afterExecute(r, t);
        try {
            GormUtil.closeSession();
        } catch (Exception e) {
            log.error("Failed to close Hibernate session: " + e.getMessage(), e);
        }
        if (t != null) {
            log.error("Exception thrown by task " + r + ": " + t.getMessage(), t);
        }
    }
}

/* function that maps a CVTerm to a set of persisted Statements, ElementAnnotations
and RevisionAnnotations */
final class CVTermToAnnotationStatementConverter implements
        Function<TransactionStatus, Set<ElementAnnotation>> {
    private Logger logger = LoggerFactory.getLogger("CVTermToAnnotationStatementConverter");
    private final CVTerm term;
    private final ModelElementTypeResolver typeResolver;
    private final long revisionId;
    private final String author;

    CVTermToAnnotationStatementConverter(CVTerm term, ModelElementTypeResolver typeResolver,
        long revisionId, String author) {
        this.term = term;
        this.typeResolver = typeResolver;
        this.revisionId = revisionId;
        this.author = author;
    }

    @Override
    public Set<ElementAnnotation> apply(TransactionStatus transactionStatus) {
        try {
            return new ElementAnnotationFactory(term, typeResolver,
                revisionId, author).build();
        } catch (Throwable t) {
            transactionStatus.setRollbackOnly();
            final String q = term.getQualifier().toString();
            final String xrefs = term.getResources().toString();
            final String subject = SbmlUtils.getSBaseForCVTerm(term).getMetaId();
            final String annos = new StringBuilder()
                .append(subject).append(" ").append(q).append(" ").append(xrefs).toString();
            logger.error("Could not save annotations  " + annos, t);
        }
        return new HashSet<>();
    }
}

/* support classes for CVTermToAnnotationStatementConverter */
class UriResourceReferenceBuilder {
    private Logger logger = LoggerFactory.getLogger("UriResourceReferenceBuilder");
    // keep this protocol-relative in the odd chance these are https:// URIs.
    private final String BASE_URI = "//identifiers.org/";
    private final String DELIMITER = "/";
    private static final String DEFAULT_DATA_TYPE = "unknown";
    private String uri, accession, dataType, collectionName;

    UriResourceReferenceBuilder(@NotNull(message = "Please provide a URI") String uri) {
        this.uri = uri;
        this.accession = getAccessionFromUri(uri);
        this.dataType = getDataTypeFromUri(uri);
        String namespace = String.format("http:%s%s/", BASE_URI, dataType);
        String miriamRegistryName = MiriamRegistryService.fetchCollectionName(namespace);
        if (null != miriamRegistryName && miriamRegistryName.isEmpty()) {
            collectionName = null;
        } else {
            collectionName = miriamRegistryName;
        }
    }

    private String getDataTypeFromUri(String uri) {
        int baseUriIdx = uri.lastIndexOf(BASE_URI);
        if (-1 == baseUriIdx) {
            logger.error("Cannot detect collection for resource reference {}", uri);
            return DEFAULT_DATA_TYPE;
        }
        int accessionIdx = uri.lastIndexOf(DELIMITER + accession);
        return uri.substring(baseUriIdx + BASE_URI.length(), accessionIdx);
    }

    private String getAccessionFromUri(String uri) {
        int idx = uri.lastIndexOf(DELIMITER);
        if (idx == -1) {
            logger.error("Cannot find the accession for {}", uri);
            return null;
        }
        if (idx == uri.length()) {
            logger.error("{} is not a valid identifiers.org URI for a resource reference", uri);
            return null;
        }
        return uri.substring(idx + 1);
    }

    ResourceReference build() {
        ResourceReference result = new ResourceReference();
        result.setUri(uri);
        result.setDatatype(dataType);
        result.setAccession(accession);
        result.setCollectionName(collectionName);
        result.setResolutionStatus(ResolutionStatus.UNRESOLVED);
        return result;
    }

    public static String getDefaultDataType() {
        return DEFAULT_DATA_TYPE;
    }
}

class ElementAnnotationFactory {
    private final Map<String, Object> LOOKUP_QUERY_PARAMETERS;
    private StatementBuilder statementFactory;
    private final long revisionId;
    private static final Logger log = LoggerFactory.getLogger(ElementAnnotationFactory.class);

    ElementAnnotationFactory(CVTerm term, ModelElementTypeResolver resolver, long revisionId,
        String author) {
        this.revisionId = revisionId;
        statementFactory = new StatementBuilder(term);
        SBase entity = SbmlUtils.getSBaseForCVTerm(term);
        ModelElementType elementType = resolver.find(entity.getElementName());
        elementType = elementType.merge();
        LOOKUP_QUERY_PARAMETERS = new LinkedHashMap<>();
        if (null != elementType) {
            LOOKUP_QUERY_PARAMETERS.put("modelElementType", elementType);
        }
        LOOKUP_QUERY_PARAMETERS.put("creatorId", author);
    }

    private ElementAnnotation fromStatement(Statement statement) {
        if (null == statement || null == statement.getId()) {
            return null; // we've already dealt with the error with the statement.
        }
        LOOKUP_QUERY_PARAMETERS.put("statement", statement);
        ElementAnnotation result = null;
        try {
            result = ElementAnnotation.findOrCreateWhere(LOOKUP_QUERY_PARAMETERS);
        } catch (Exception e) {
            log.error("Failed to associate Statement #{} with revision {} -- {}",
                statement.getId(), revisionId, e.toString());
        }
        return result;
    }

    private boolean haveModelElementType() {
        return LOOKUP_QUERY_PARAMETERS.containsKey("modelElementType");
    }

    Set<ElementAnnotation> build() {
        if (!haveModelElementType()) {
            log.warn("Cannot create ElementAnnotations for undefined element type {}",
                    statementFactory.getElementIdentifier());
            return Collections.emptySet();
        }
        final Revision revision = Revision.get(revisionId);
        Set<ElementAnnotation> annotations = statementFactory.toStatements().stream()
            .map(this::fromStatement)
            .collect(Collectors.toSet());
        RevisionAnnotation inserted = null;
        try {
            for (ElementAnnotation ea : annotations) {
                if (null == ea) {
                    continue;
                }
                if (null == ea.save()) {
                    String errors = MessageSourceUtils.convertErrorsToString(ea.getErrors());
                    Long statementId = ea.getStatement().getId();
                    String sErr = MessageSourceUtils.convertErrorsToString(
                        ea.getStatement().getErrors());
                    log.error("Annotation {} (statement #{} errors: {}) is invalid: {}", ea,
                        statementId, sErr, errors);
                } else {
                    // ea is saved successfully, try map this ea to a revision
                    RevisionAnnotation toInsert = new RevisionAnnotation();
                    toInsert.setRevision(revision);
                    toInsert.setElementAnnotation(ea);
                    if (!toInsert.validate()) {
                        log.error("RA for anno {} has errors: {}", ea,
                            MessageSourceUtils.convertErrorsToString(toInsert.getErrors()));
                    } else {
                        LinkedHashMap<String, Boolean> saveParams = new LinkedHashMap<>();
                        saveParams.put("validate", Boolean.FALSE);
                        saveParams.put("flush", Boolean.FALSE);
                        inserted = toInsert.save(saveParams);
                    }
                }
            }
        } catch (DataIntegrityViolationException e) {
            log.error("Failed to save ElementAnnotations " + annotations, e);
            throw new NamedTransactionRollbackException("Cannot save annotations " + annotations, e);
        }

        return annotations;
    }
}

class ModelElementTypeResolver {
    protected final ModelFormat format;

    ModelElementTypeResolver(ModelFormat format) {
        if (!TransactionSynchronizationManager.hasResource(GormUtil.getSessionFactory())) {
            throw new IllegalStateException("Expected a Hibernate session to already be bound");
        }
        this.format = format;
    }

    ModelElementType find(String name) {
        Map<String, Object> properties = new HashMap<>(5, 1.0f);
        properties.put("name", name);
        properties.put("format", format);
        properties.put("readOnly", true);
        properties.put("max", 1);
        List<ModelElementType> modelElementTypes = ModelElementType.executeQuery(
            "from ModelElementType where name = :name and modelFormat = :format", properties);
        return modelElementTypes.size() > 0 ? modelElementTypes.get(0) : null;
    }
}

class StatementBuilder {
    private static final int VARCHAR_LIMIT = 255;
    private static final Logger log = LoggerFactory.getLogger(StatementBuilder.class);

    // keys: subjectId, qualifier and object. the latter is modified for each ResourceReference.
    private final Map<String, Object> STATEMENT_LOOKUP_PROPERTIES = new HashMap<>();
    private Stream<String> referenceUriStream;

    StatementBuilder(CVTerm term) {
        String subject = getSubjectIdFromCVTerm(term);
        STATEMENT_LOOKUP_PROPERTIES.put("subjectId", subject);
        String qualifierUri = SbmlUtils.getUriForQualifier(term);
        Qualifier qualifier = Qualifier.findWhere(Collections.singletonMap("uri", qualifierUri));
        STATEMENT_LOOKUP_PROPERTIES.put("qualifier", qualifier);
        referenceUriStream = term.getResources().stream();
    }

    private String getSubjectIdFromCVTerm(CVTerm term) {
        String metaId = SbmlUtils.getSBaseForCVTerm(term).getMetaId();
        String subjectId;
        // Statement.subjectId is VARCHAR(255) so that we can have an index on the column.
        if (metaId.length() > VARCHAR_LIMIT) {
            subjectId = metaId.substring(0, VARCHAR_LIMIT);
            log.warn("MetaId {} will be truncated to 255 characters: {}", metaId, subjectId);
        } else {
            subjectId = metaId;
        }
        return subjectId;
    }

    private ResourceReference resolveResourceReference(String uri) {
        ResourceReference result = null;
        try {
            result = ResourceReference.findWhere(Collections.singletonMap("uri", uri));
        } catch (Exception e) {
            log.error("Exception encountered while looking up cross reference {}: {}", uri, e);
        }
        return result;
    }

    private Statement statementForReference(ResourceReference r) {
        if (null == r) {
            log.warn("Undefined xref for subject {} and qualifier {} -- cannot save statement.",
                    STATEMENT_LOOKUP_PROPERTIES.get("subjectId"),
                    ((Qualifier) STATEMENT_LOOKUP_PROPERTIES.get("qualifier")).getUri());
            return null;
        }
        STATEMENT_LOOKUP_PROPERTIES.put("object", r);
        Statement result = null;
        try {
            result = Statement.findOrSaveWhere(STATEMENT_LOOKUP_PROPERTIES);
        } catch (Exception e) {
            log.error("Exception while looking up statement for subject {} and qualifier {}",
                    STATEMENT_LOOKUP_PROPERTIES.get("subjectId"),
                    ((Qualifier) STATEMENT_LOOKUP_PROPERTIES.get("qualifier")).getUri());
        }
        return result;
    }

    Set<Statement> toStatements() {
        return referenceUriStream
            .map(this::resolveResourceReference)
            .map(this::statementForReference)
            .collect(Collectors.toSet());
    }

    String getElementIdentifier() {
        return (String) STATEMENT_LOOKUP_PROPERTIES.get("subjectId");
    }
}

/* utility class to simplify writing transactional code */
/* provides similar semantics to withTransaction, but works from Java */
/* the closure-based callbacks from Grails are replaced by Java8 Functions*/
final class TxSupport {
    private static final Logger log = LoggerFactory.getLogger(TxSupport.class);
    private final PlatformTransactionManager manager;

    TxSupport() {
        try {
            this.manager = GormUtil.getBean("transactionManager",
                PlatformTransactionManager.class);
        } catch (Throwable t) {
            throw new IllegalStateException(
                "Cannot find platform transaction manager, ensure GormUtil.init() was called",
                t);
        }
    }

    static TransactionDefinition makeNamedTxDefinition(String name) {
        DefaultTransactionDefinition result = new DefaultTransactionDefinition();
        result.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
        if (name != null && !name.trim().isEmpty()) {
            result.setName(name);
        }
        return result;
    }

    static TransactionDefinition makeNamedReadOnlyTxDefinition(String name) {
        DefaultTransactionDefinition result =
            (DefaultTransactionDefinition) makeNamedTxDefinition(name);
        result.setReadOnly(true);
        return result;
    }

    <T> T withNamedTransactionTemplate(TransactionDefinition transactionDefinition,
        Function<TransactionStatus, T> action) {
        T result;
        TransactionStatus tx = manager.getTransaction(transactionDefinition);
        try {
            result = action.apply(tx);
            manager.commit(tx);
            return result;
        } catch (DataIntegrityViolationException t) {
            tx.setRollbackOnly();
            String name = transactionDefinition.getName();
            log.error("damn -- " + name, t);
            throw new NamedTransactionRollbackException(name, t); // let client deal with it
        }
    }
}

class NamedTransactionRollbackException extends RuntimeException {
    private static final String DEFAULT_MESSAGE_TPL = "Rolled back named transaction ";

    @SuppressWarnings("unused")
    NamedTransactionRollbackException(String name) {
        this(name, DEFAULT_MESSAGE_TPL, null);
    }

    NamedTransactionRollbackException(String name, Throwable t) {
        this(name, DEFAULT_MESSAGE_TPL, t);
    }

    @SuppressWarnings("WeakerAccess")
    NamedTransactionRollbackException(String name, String message, Throwable cause) {
        super(message + name, cause);
    }
}

/* java8 functions that are used in conjunction with TxSupport */
class FindQualifierId extends FindQualifierByQuery<Long> {
    private static final String query = "select id from Qualifier where uri = :uri";

    FindQualifierId(String uri) {
        super(query, Collections.singletonMap("uri", uri));
    }
}

class FindQualifierByQuery<T> implements Function<TransactionStatus, T> {
    private static final String DEFAULT_QUERY = "from Qualifier where uri = :uri";
    private final String query;
    private final Map<String, Object> queryProperties;

    @SuppressWarnings("unused")
    FindQualifierByQuery(Map<String, Object> properties) {
        this(DEFAULT_QUERY, properties);
    }

    FindQualifierByQuery(String query, Map<String, Object> properties) {
        Objects.requireNonNull(query);
        this.query = query;
        queryProperties = new HashMap<>(properties.size() + 2, 1.0f);
        queryProperties.putAll(properties);
        queryProperties.put("max", 1);
        queryProperties.put("readOnly", true);
    }

    @Override
    public T apply(TransactionStatus status) {
        List<Qualifier> matches = Qualifier.executeQuery(query, queryProperties);
        if (matches.size() > 0) {
            //noinspection unchecked
            return (T) matches.get(0);
        }
        return null;
    }
}

class AttributeBasedResourceReferenceFinder<T> implements Function<TransactionStatus, T> {
    private final String query;
    private final Map<String, Object> queryProperties;

    AttributeBasedResourceReferenceFinder(String query, Map<String, Object> properties) {
        Objects.requireNonNull(query);
        Objects.requireNonNull(properties);
        this.query = query;
        queryProperties = new HashMap<>(properties.size() + 2, 1.0f);
        queryProperties.putAll(properties);
        queryProperties.put("max", 1);
        queryProperties.put("readOnly", true);
    }

    @Override
    public T apply(TransactionStatus status) {
        List<ResourceReference> result = ResourceReference.executeQuery(query, queryProperties);
        if (result.size() > 0) {
            //noinspection unchecked
            return (T) result.get(0);
        }
        return null;
    }
}

class FindResourceReferenceByUri extends AttributeBasedResourceReferenceFinder<Long> {
    FindResourceReferenceByUri(String uri) {
        super("select id from ResourceReference rr where rr.uri = :uri",
            Collections.singletonMap("uri", uri));
    }
}

class InsertResourceReferenceQuery implements Function<TransactionStatus, ResourceReference>,
        Consumer<TransactionStatus> {
    private static final Logger log = LoggerFactory.getLogger(InsertResourceReferenceQuery.class);

    final ResourceReference reference;

    InsertResourceReferenceQuery(String uri) {
        this.reference = new UriResourceReferenceBuilder(uri).build();
    }

    @Override
    public ResourceReference apply(TransactionStatus transactionStatus) {
        return doInsert(transactionStatus);
    }

    private ResourceReference doInsert(TransactionStatus status) {
        try {
            if (null == reference.save()) {
                log.error("Failed to persist resource reference {}: {} ", reference.getUri(),
                    MessageSourceUtils.convertErrorsToString(reference.getErrors()));
                reference.discard();
            }
        } catch (Throwable t) {
            status.setRollbackOnly();
            final String r = reference.getUri();
            log.error("Exception encountered while attempting to save resource reference " + r, t);
        }
        return reference;
    }

    @Override
    public void accept(TransactionStatus transactionStatus) {
        doInsert(transactionStatus);
    }
}

class InsertQualifierQuery implements Consumer<TransactionStatus>,
        Function<TransactionStatus, Qualifier> {
    private static final Logger log = LoggerFactory.getLogger(InsertQualifierQuery.class);
    private static final String DEFAULT_TYPE = "unknown";
    private static final String DEFAULT_NS = "unknown";
    private final String uri;
    private final String accession;
    private final String namespace;
    private final String type;

    InsertQualifierQuery(String uri) {
        Objects.requireNonNull(uri);
        this.uri = uri;
        String delimiter = uri.contains("/") ? "/" : uri.contains(":") ? ":" : "?";
        String[] uriParts = uri.split(delimiter);
        int length = uriParts.length;
        if (1 == length) {
            accession = "";
            namespace = DEFAULT_NS;
            this.type = DEFAULT_TYPE;
        } else {
            accession = uriParts[length - 1];
            namespace = uri.substring(0, uri.length() - accession.length());
            boolean isBQ = namespace.equals(CVTerm.URI_BIOMODELS_NET_BIOLOGY_QUALIFIERS);
            boolean isMQ = namespace.equals(CVTerm.URI_BIOMODELS_NET_MODEL_QUALIFIERS);
            this.type = isBQ ? CVTerm.URI_BIOMODELS_NET_BIOLOGY_QUALIFIERS :
                isMQ ? CVTerm.URI_BIOMODELS_NET_MODEL_QUALIFIERS : DEFAULT_TYPE;
        }
    }

    @Override
    public void accept(TransactionStatus transactionStatus) {
        doInsert();
    }

    @Override
    public Qualifier apply(TransactionStatus transactionStatus) {
        return doInsert();
    }

    private Qualifier doInsert() {
        Qualifier q = buildQualifier();
        if (null == q.save()) {
            log.error("Failed to persist qualifier {}: {}", q.getUri(),
                MessageSourceUtils.convertErrorsToString(q.getErrors()));
            q.discard();
        }
        return q;
    }

    private Qualifier buildQualifier() {
        Qualifier result = new Qualifier();
        result.setAccession(accession);
        result.setNamespace(namespace);
        result.setUri(uri);
        result.setQualifierType(type);
        return result;
    }
}
