/*
 * Copyright (C) 2010-2020 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing;

import java.util.Arrays;
import java.util.Optional;

/**
 * Enum for the types of supported data export strategies.
 *
 * <p>Each entry has
 * <ul>
 *  <li>an option name which denotes the value command-line argument that has been passed to the indexer</li>
 *  <li>a fileNamePrefix representing the prefix of the output files produced by the corresponding exporter</li>
 * </ul>
 * </p>
 * @see net.biomodels.jummp.indexing.exporter.ModelExporter
 */
public enum ExportType {
    // files must be named OmicsDIEntries* in order for the OmicsDI pipeline to process them
    OMICS_DI("OmicsDIXml", "OmicsDIEntries"),
    EBI_SEARCH("EBISearch", "EBISearch");

    final String optionName;
    final String fileNamePrefix;

    ExportType(String option, String prefix) {
        this.optionName = option;
        this.fileNamePrefix = prefix;
    }

    // workaround for https://issues.apache.org/jira/browse/GROOVY-9007
    public String getFileNamePrefix() { return fileNamePrefix; }

    public static ExportType forOption(final String s) {
        Optional<ExportType> match = Arrays.stream(ExportType.values())
            .filter( e -> e.optionName.equals(s) )
            .findAny();
        return match.orElse(null);
    }
}
