/**
 * Copyright (C) 2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/
package net.biomodels.jummp.indexing

import groovyx.gpars.GParsPool
import net.biomodels.jummp.annotationstore.ResolutionStatus
import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.indexing.miriam.MiriamRegistryService
import org.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.client.HttpClientErrorException
import org.xml.sax.SAXException
import uk.ac.ebi.pride.utilities.ols.web.service.model.Identifier
import uk.ac.ebi.pride.utilities.ols.web.service.model.Term

import java.nio.file.FileSystems
import java.nio.file.Path
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CountDownLatch
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger

/**
 * Utility for updating resource references from the database.
 *
 * This class uses AnnotationReferenceResolver to update the URI, data type and collection name
 * of the cross references persisted during indexing.
 *
 * AnnotationReferenceResolver processors use the following template for resolving cross references
 *    if it's a supported collection type
 *      check if we've already resolved a cross reference with this URI
 *      if yes
 *        load it from the database into the thread-local cache and return it
 *      if not
 *        resolve it
 *        save the resulting information
 *        update the cross reference cache
 *
 * This workflow doesn't suit our batch resource reference updater, because
 *    a) we need to update the existing data
 *    b) we don't need to cache the result because each cross reference is only processed once.
 *
 * We therefore subclass the existing TermInformationProvider implementations and force
 * AnnotationReferenceResolver to use them instead of their caching counterparts.
 *
 * We process the Ensembl cross references separately from the rest because the Ensembl REST API
 * has a hard limit of 15 requests/second per client (IP address) and any attempts to go over the
 * limit result in HTTP Status code 429 -- Too many requests. Consequently, we use a
 * {@link java.util.concurrent.ScheduledExecutorService} which every 2 seconds takes a batch of 15
 * Ensembl xrefs and attempts to look them up. If the API throws an exception, we must sleep for a
 * period of time, otherwise we'll run into the rate limiter.
 *
 * One potential improvement for the Ensembl resolver is to use the POST /id/lookup endpoint,
 * http://rest.ensembl.org/documentation/info/lookup_post, since it allows us to query up to 1000
 * cross references in the same request.
 */
class BatchResourceReferenceResolver {
    private final Logger logger = LoggerFactory.getLogger(BatchResourceReferenceResolver.class)
    private AnnotationReferenceResolver resolver
    private RequestContext ctx
    private ConcurrentHashMap<String, TermInformationProvider> collectionTermInformationProviders
    private final String ensemblCollectionName = 'ensembl'

    BatchResourceReferenceResolver() {
        this(getDefaultPropertiesPath())
    }

    BatchResourceReferenceResolver(String propertiesPath) {
        if (!propertiesPath) {
            throw new IllegalArgumentException("Cannot find .jummp.properties")
        }

        initialiseGorm(propertiesPath)
        setUpAnnotationReferenceResolver()
        logger.trace("Initialised using settings from $propertiesPath")
    }

    private void initialiseGorm(String configPath) {
        ctx = new RequestContext()
        ctx.setConfigFilePath(configPath)
        ctx.setPartialData([:])
        GormUtil.initGorm(ctx)
    }

    private void setUpAnnotationReferenceResolver() {
        resolver = new AnnotationReferenceResolver()
        resolver.setOnline(true)
        resolver.onlineProcessors = [
                new NonCachingOLSFetcher(),
                new NonCachingEnsemblFetcher(),
                new NonCachingInterProFetcher(),
                new NonCachingKeggFetcher(),
                new NonCachingReactomeFetcher(),
                new NonCachingUniProtFetcher()
        ]

        logger.trace("Using online processors {}", resolver.onlineProcessors)
    }

    private static String getDefaultPropertiesPath() {
        String preferredLocation = System.getenv("JUMMP_CONFIG")
        Path preferredPath
        if (null == preferredLocation) {
            String home = System.getProperty("user.home")
            Path defaultPath = FileSystems.getDefault().getPath(home, ".jummp.properties")
            if (!defaultPath.toFile().exists()) {
                throw new IllegalStateException("jummp properties file missing")
            }
            preferredPath = defaultPath
        } else {
            preferredPath = FileSystems.default.getPath(preferredLocation)
        }
        preferredPath.toString()
    }

    static void main(String[] args) {
        BatchResourceReferenceResolver self
        if (args?.length) {
            self = new BatchResourceReferenceResolver(args[0])
        } else {
            self = new BatchResourceReferenceResolver()
        }
        self.setup()
        try {
            GParsPool.withPool(2) {
                self.processNonEnsemblResourceReferences()
                self.processEnsemblCrossReferences()
            }
        } finally {
            self.stop()
        }
    }

    void processEnsemblCrossReferences() {
        final int BATCH_SIZE = 15
        List<Long> referenceIDs = ResourceReference.executeQuery(
                "select id from ResourceReference where collectionName = '$ensemblCollectionName' and name is null")
        List<List<Long>> referenceBatches = ListPartitionSupport.partitionUsingStreams(
                referenceIDs, BATCH_SIZE)
        final int BATCH_COUNT = referenceBatches.size()
        def scheduledPool = Executors.newScheduledThreadPool(1)
        AtomicInteger batchCounter = new AtomicInteger()
        CountDownLatch shouldStopEnsemblResolver = new CountDownLatch(BATCH_COUNT)
        AtomicBoolean resolversWereInterrupted = new AtomicBoolean(false)
        scheduledPool.scheduleAtFixedRate({
            int index = batchCounter.get()
            if (index == BATCH_COUNT) {
                return
            }
            ResourceReference r
            GormUtil.openSession()
            try {
                def xrefIDs = referenceBatches.get(index)
                for (Long id : xrefIDs) {
                    r = ResourceReference.get(id)
                    try {
                        updateReference r
                    } catch (Exception e) {
                        logger.error("Ensembl exception for {}: {}", r.accession, e.message)
                        Thread.sleep(1500)
                    }
                }
            } catch (InterruptedException ignore) {
                resolversWereInterrupted.lazySet(true) // clean up before stopping
            } finally {
                if (GormUtil.isSessionAlreadyBound()) {
                    GormUtil.closeSession()
                }
                batchCounter.lazySet(index + 1)
                shouldStopEnsemblResolver.countDown()
                if (resolversWereInterrupted.get()) {
                    logger.warn "Interrupted"
                    Thread.currentThread().interrupt()
                }
            }
        }, 1L, 2L, TimeUnit.SECONDS)

        while (!shouldStopEnsemblResolver.await(10, TimeUnit.MICROSECONDS) &&
                !resolversWereInterrupted.get()) {
            Thread.sleep(50)
        }
        List<Runnable> interruptedTasks = scheduledPool.shutdownNow()
        if (interruptedTasks) {
            // because each worker thread may have up to BATCH_SIZE references to resolve.
            logger.error("Failed to resolve approx {} ENSEMBL cross references",
                    BATCH_SIZE * interruptedTasks.size())
        }
        if (!scheduledPool.awaitTermination(1L, TimeUnit.SECONDS)) {
            logger.error "Failed to stop Ensembl resolver workers"
        }
    }

    void processNonEnsemblResourceReferences() {
        def all = ResourceReference.count()
        List<Long> referenceIDs = ResourceReference.executeQuery(
                "select id from ResourceReference where (collectionName !='$ensemblCollectionName' or collectionName is null) and name is null")
        def referenceBatches = ListPartitionSupport.partitionUsingStreams(referenceIDs, 100)
        logger.info("Resolving ${referenceIDs.size()} out of $all cross references.")
        int poolSize = Math.min(2 * Runtime.runtime.availableProcessors(), GormUtil.MAX_POOL_SIZE)
        GParsPool.withPool (poolSize) {
            referenceBatches.eachParallel { List<Long> references ->
                GormUtil.openSession()
                try {
                    for (Long id : references) {
                        ResourceReference r = ResourceReference.get id
                        try {
                            updateReference r
                        } catch (Exception e) {
                            logger.error("Failed to update xref {} ({}): {}", id, r.uri, e.message)
                        }
                    }
                } finally {
                    try {
                        GormUtil.sessionFactory.currentSession.flush()
                    } finally {
                        GormUtil.closeSession()
                    }
                }
            }
        }
    }
    private String getUriWithLowerCaseDataType(String currentURI,AnnotationReferenceResolver.AnnoType xrefType) {
        String[] dataTypeAndAccession = resolver.extractParts(currentURI, xrefType)
        String baseUrl = resolver.getBaseUrlByAnnoType(xrefType)
        String dataType = dataTypeAndAccession[0].toLowerCase()
        String accession = dataTypeAndAccession[1]
        if (null == baseUrl || !dataType || !accession ) {
            return currentURI
        }
        String lowerCaseUri = "$baseUrl$dataType/$accession"
        logger.debug "Converted to lowercase: $lowerCaseUri"
        return lowerCaseUri

    }

    void updateReference(ResourceReference reference) {
        String currentURI = reference.uri
        AnnotationReferenceResolver.AnnoType xrefType = resolver.getType(currentURI)
        currentURI = getUriWithLowerCaseDataType(currentURI, xrefType)
        logger.info "Processing $currentURI"
        if (xrefType == AnnotationReferenceResolver.AnnoType.UNKNOWN) {
            return
        }

        // update the URI, dataType and collection name if necessary
        // we need to do this to avoid problems with obsolete collections .e.g. obo.go instead of go
        String updatedURI = MiriamRegistryService.updateURI currentURI
        logger.info "Updated URI: $updatedURI through MIRIAM Registry"
        if (!updatedURI) {
            reference.setResolutionStatus(ResolutionStatus.UNRESOLVABLE)
            saveReference(reference)
            logger.warn "{} {} is invalid according to the MIRIAM Registry information",
                    reference, reference.uri
            return
        }
        if (updatedURI && updatedURI != reference.uri) {
            logger.trace("URI update for ResourceReference {}: {} -> {}", reference.id, currentURI,
                    updatedURI)
            ResourceReference duplicate = ResourceReference.findByUri(updatedURI)
            if (duplicate) {
                logger.warn "ResourceReference {} ({}) should be replaced by #{} ({}). Skipping.",
                        reference.id, currentURI, duplicate.id, duplicate.uri
                return
            }
            reference.uri = updatedURI
        }
        String[] dataTypeAndAccession = resolver.extractParts(updatedURI, xrefType)
        String expectedDataType = dataTypeAndAccession[0]
        String expectedAccession = dataTypeAndAccession[1]
        def collectionName = MiriamRegistryService.fetchCollectionNameByType(expectedDataType)
        if (!expectedDataType || !expectedAccession || !collectionName) {
            reference.setResolutionStatus(ResolutionStatus.UNRESOLVABLE)
            saveReference(reference)
            logger.error "MiriamRegistryService returned type {} and collection name {} for {}",
                    reference.uri, expectedDataType, collectionName
        } else {
            if (expectedDataType != reference.datatype) {
                logger.trace("Updating data type for {} to {}", reference.uri, expectedDataType)
                reference.datatype = expectedDataType
            }
            if (collectionName != reference.collectionName) {
                logger.trace("Updating collection name for {} to {}", reference.uri, collectionName)
                reference.collectionName = collectionName
            }
            if (expectedAccession != reference.accession) {
                logger.trace("Updating accession for {} to {}", reference.uri, expectedAccession)
                reference.accession = expectedAccession
            }
        }
        logger.info "Checking if reference object has changed"
        def provider = getProviderFor(reference.datatype)
        if (provider) {
            reference = provider.getTermInformation(reference)
            if (reference.isDirty() || reference.hasChanged()) {
                saveReference(reference)
            } else {
                logger.info("Leaving {} as is", reference.uri)
            }
        }
    }

    private void saveReference(ResourceReference reference) {
        if (!reference.save()) {
            logger.error("Failed to update reference {}: {}", reference.id,
                reference.errors.allErrors)
        } else {
            logger.debug("Updated resource reference {} ({})", reference.id, reference.uri)
        }
    }
    @SuppressWarnings("GrMethodMayBeStatic")
    void setup() {
        GormUtil.openSession()
        collectionTermInformationProviders = new ConcurrentHashMap<>()
    }

    private TermInformationProvider getProviderFor(String dataType) {
        if ("pubmed" == dataType || "biomodels.db" == dataType) {
            return null
        }
        def result = collectionTermInformationProviders.get(dataType)
        if (!result) {
            logger.trace("TIP cache miss for {} :(", dataType)
            TermInformationProvider provider = resolver.getTermInformationProvider(dataType)
            if (provider) {
                result = collectionTermInformationProviders.putIfAbsent(dataType, provider)
            }
            if (!result) {
                result = provider
            }
        }
        result
    }

    void stop() {
        GormUtil.closeSession()
        resolver.shutdown()
    }
}

class NonCachingOLSFetcher extends OLSDirectFetcher {
    def logger = LoggerFactory.getLogger(NonCachingOLSFetcher.class)

    private static String convertTermIdentifier(String id, String collType) {
        if (null == id || null == collType)
            return id
        if (collType.equalsIgnoreCase("ncit")) {
            return "NCIT:$id"
        } else if(collType.equalsIgnoreCase("efo")) {
            return "EFO:$id"
        }
        return id
    }
    @Override
    ResourceReference getTermInformation(ResourceReference reference) {
        String id = reference.getAccession()
        String collType = reference.getDatatype()
        id = convertTermIdentifier(id, collType)
        Identifier termIdentifier = new Identifier(id, Identifier.IdentifierType.OBO)
        Term term
        try {
            term = olsClient.getTermById(termIdentifier, collType)
            if (term.getAnnotation().containsAnnotation("term replaced by")) {
                String replacedByUrl = term.getAnnotation().getAnnotation("term replaced by").get(0)
                replacedByUrl = replacedByUrl.substring(replacedByUrl.lastIndexOf("/") + 1)
                replacedByUrl = replacedByUrl.replace("_", ":")
                termIdentifier.setIdentifier(replacedByUrl)
                term = olsClient.getTermById(termIdentifier, collType)
            }
            if (term) {
                String label = term.label
                String[] des = term.description
                reference.setName(label)
                reference.setResolutionStatus(ResolutionStatus.RESOLVED)
                logger.trace("Setting name of {} to {}", reference.id, label)
                if (des) {
                    reference.setDescription(des[0])
                }
            }
        } catch (IOException ie) {
            logger.error("IO error is returned while resolving $id from $collType", ie.message)
            reference.setResolutionStatus(ResolutionStatus.RESOLVER_ERROR)
        } catch (HttpClientErrorException he) {
            logger.error("OLS API returned a HTTP error while resolving $id from $collType", he.message)
            reference.setResolutionStatus(ResolutionStatus.UNRESOLVABLE)
        } catch (Exception e) {
            logger.error("OLS API returned an error while resolving $id from $collType", e.message)
            reference.setResolutionStatus(ResolutionStatus.RESOLVER_ERROR)
        }
    reference
}

}

class NonCachingEnsemblFetcher extends EnsemblDatabaseFetcher {
    @Override
    ResourceReference getTermInformation(ResourceReference reference) {
        String id = reference.getAccession()
        URL url = new URL("${super.server}${id}")

        URLConnection connection = url.openConnection()
        HttpURLConnection httpConnection = (HttpURLConnection)connection

        httpConnection.setRequestProperty("Content-Type", "application/json")

        InputStream response = connection.getInputStream()
        int responseCode = httpConnection.getResponseCode()

        if(responseCode != 200) {
            throw new RuntimeException("Response code was not 200. Detected response was $responseCode")
        }

        String output
        Reader reader = null
        try {
            reader = new BufferedReader(new InputStreamReader(response, "UTF-8"))
            StringBuilder builder = new StringBuilder()
            char[] buffer = new char[8192]
            int read
            while ((read = reader.read(buffer, 0, buffer.length)) > 0) {
                builder.append(buffer, 0, read)
            }
            output = builder.toString()
        }
        finally {
            if (reader != null) try {
                reader.close()
            } catch (IOException e) {
                logger.error("Failed to resolve Ensembl id {}: {}", id, e.message)
            }
        }
        if (output) {
            JSONObject json = new JSONObject(output)
            if (json.has("display_name")) {
                String name = json.getString("display_name").toString()
                if (name) {
                    reference.setName(name)
                }
            }
        }

        reference
    }
}

class NonCachingInterProFetcher extends InterProDatabaseFetcher {
    @Override
    ResourceReference getTermInformation(ResourceReference reference) {
        String id = reference.getAccession()
        if (!entryList.isEmpty()) {
            String name = entryList.get(id)
            if (name) {
                reference.setName(name)
            }
        }

        reference
    }
}

class NonCachingKeggFetcher extends KeggDatabaseFetcher {

    URLConnection openURLConnection(URL url)throws IOException {
        return url.openConnection()
    }

    @Override
    ResourceReference getTermInformation(ResourceReference reference) {
        String id = reference.getAccession()
        String name = ""
        String url  = "http://rest.kegg.jp/get/${id}"
        URL urlObj = new URL (url)
        URLConnection urlConn = openURLConnection(urlObj)
        if (urlConn.responseCode == 200) {
            try {
                def response = urlObj.getText()
                String type = ""
                // kegg.glycan doesn't have NAME property, see https://www.kegg.jp/entry/G13032
                // the other kegg data-oriented entry points use NAME property
                String nameLine = response.readLines().find { it =~ /NAME|COMPOSITION|DEFINITION/ }
                if (nameLine) {
                    def cond = (nameLine =~ /NAME|COMPOSITION|DEFINITION/)
                    type = cond.getAt(0)
                    String[] names = nameLine.split(type)
                    names = names - ""
                    name = names[0].endsWith(';') ? names[0] - ';' : names[0]
                    name = name.trim()
                }
                if (name) {
                    reference.setName(name)
                    reference.setResolutionStatus(ResolutionStatus.RESOLVED)
                }
            } catch (IOException ioe) {
                logger.error("I/O error while resolving KEGG term $id: {}", ioe.message)
                reference.setResolutionStatus(ResolutionStatus.RESOLVER_ERROR)
            } catch (Exception e) {
                logger.error("Error while resolving KEGG term $id: {}", e.message)
                reference.setResolutionStatus(ResolutionStatus.RESOLVER_ERROR)
            }
        } else{
            reference.setResolutionStatus(ResolutionStatus.UNRESOLVABLE)
        }
        reference
    }
}

class NonCachingReactomeFetcher extends ReactomeDatabaseFetcher {

    URLConnection openURLConnection(URL url)throws IOException {
        return url.openConnection()
    }
    @Override
    ResourceReference getTermInformation(ResourceReference reference) {
        String id = reference.accession
        //https://www.reactome.org/ContentService/data/query/R-HSA-179863/displayName
        String url = "http://www.reactome.org/ContentService/data/query/${id}/displayName"
        URL urlObject = new URL(url)
        URLConnection urlConnection = openURLConnection(urlObject)
        int responseCode = urlConnection.responseCode
        if (responseCode == 200) {
            String name
            try {
                name = urlObject.text
            } catch (IOException e) {
                logger.error("Reactome API threw an I/O exception for {}: {}", id, e.message)
                reference.setResolutionStatus(ResolutionStatus.RESOLVER_ERROR)
            }
            if (name) {
                reference.name = name
                reference.setResolutionStatus(ResolutionStatus.RESOLVED)
            } else {
                logger.warn("Reactome API didn't return a label for {}", id)
                reference.setResolutionStatus(ResolutionStatus.UNRESOLVABLE)
            }
        } else{
            logger.warn("Reactome API returned statuscode: {}", responseCode)
            reference.setResolutionStatus(ResolutionStatus.UNRESOLVABLE)
        }
        reference
    }
}

class NonCachingUniProtFetcher extends UniprotDatabaseFetcher {
    def parseXMLText(String r) {
        return new XmlSlurper().parseText(r)
    }
    @Override
    ResourceReference getTermInformation(ResourceReference reference) {
        String accession = reference.getAccession()
        String collType = reference.getDatatype()
        String url
        String name = ""
        def response = null
        if (collType.equalsIgnoreCase("uniprot")) {
            url = "https://www.uniprot.org/uniprot/${accession}.xml"
            try {
                String r = new URL(url).text
                if (r) {
                    response = parseXMLText(r)
                } else {
                    reference.setResolutionStatus(ResolutionStatus.UNRESOLVABLE)
                    logger.warn("Could not resolve UniProt accession $accession")
                    response = null
                }
            } catch (Exception e) {
                reference.setResolutionStatus(ResolutionStatus.RESOLVER_ERROR)
                logger.error("Failed to resolve UniProt term {}: {}", accession, e.toString())
            }
            /**
             * Pay attention to the XML structure of UniProt returned value
             * At this time implementing this method, a UniProt document has entry as the root tag.
             * Because we need its full name, we need to point to fullName tag directly.
             */
            def protein = response?.entry?.protein
            name = protein?.recommendedName?.fullName?.text()
            if (!name) {
                name = protein?.submittedName?.text()
            }
        } else if (collType.equalsIgnoreCase("taxonomy")) {
            url = "https://www.uniprot.org/taxonomy/${accession}.rdf"
            try {
                response = new XmlSlurper().parse(url)
            } catch (IOException | SAXException e) {
                reference.setResolutionStatus(ResolutionStatus.RESOLVER_ERROR)
                logger.error("Failed to resolve Taxonomy term {} using UniProt: {} ", accession,
                    e.toString())
            }
            name = response?.Description?.scientificName
        } else if (collType.equalsIgnoreCase("ec-code")) {
            try {
                url = "https://www.uniprot.org/uniprot/?query=ec:${accession}&limit=1&format=xml"
                response = new XmlSlurper().parse(url)
            } catch (IOException | SAXException e) {
                reference.setResolutionStatus(ResolutionStatus.RESOLVER_ERROR)
                logger.error("Failed to resolve EC code {}: {}", accession, e.toString())
            }
            name = response?.entry?.protein?.recommendedName?.fullName?.text()
            if (!name) {
                name = response?.entry?.protein?.submittedName?.text()
            }
        }
        if (name) {
            reference.setName(name)
            reference.setResolutionStatus(ResolutionStatus.RESOLVED)
        }
        reference
    }
}
