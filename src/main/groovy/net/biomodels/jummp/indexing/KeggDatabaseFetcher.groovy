/**
 * Copyright (C) 2010-2017 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing

import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.indexing.miriam.MiriamRegistryService

/**
 * @author tnguyen@ebi.ac.uk on 26/04/17.
 */
class KeggDatabaseFetcher extends DatabaseBasedAnnoProcessor {
    static def supportedDataTypes = ["kegg.compound": "KEGG COMPOUND",
                                     "kegg.drug": "KEGG DRUG",
                                     "kegg.genes": "KEGG GENES",
                                     "kegg.glycan": "KEGG GLYCAN",
                                     "kegg.pathway": "KEGG PATHWAY",
                                     "kegg.orthology": "KEGG ORTHOLOGY",
                                     "kegg.reaction": "KEGG REACTION"]

    ResourceReference getTermInformation(ResourceReference reference) {
        String accession = reference.getAccession()
        String collType = reference.getDatatype()
        if (!cached.containsKey(accession) && !loadFromDatabaseIntoCache(accession, collType)) {
            String name = ""
            String url  = "http://rest.kegg.jp/get/${accession}"
            URL urlObj = new URL (url)
            URLConnection urlConn = urlObj.openConnection()
            if (urlConn.responseCode == 200) {
                try {
                    def response = urlObj.getText()
                    String type = ""
                    // kegg.glycan doesn't have NAME property, see https://www.kegg.jp/entry/G13032
                    // the other kegg data-oriented entry points use NAME property
                    String nameLine = response.readLines().find { it =~ /NAME|COMPOSITION|DEFINITION/ }
                    if (nameLine) {
                        def cond = (nameLine =~ /NAME|COMPOSITION|DEFINITION/)
                        type = cond.getAt(0)
                        String[] names = nameLine.split(type)
                        names = names - ""
                        name = names[0].endsWith(';') ? names[0] - ';' : names[0]
                        name = name.trim()
                    }
                    if (name) {
                        reference.setName(name)
                    }
                } catch (IOException ioe) {
                    logger.error("I/O error while resolving KEGG term $accession: {}", ioe.message)
                }
            }
            String collection = MiriamRegistryService.fetchCollectionNameByType(collType)
            reference.setCollectionName(collection)
            saveResource(reference)
        }
        else {
            Long cachedXrefId = cached.get(accession)
            reference = ResourceReference.get(cachedXrefId)
            if (!reference) {
                logger.error("Could not obtain term information about xref {} ({})", accession, collType)
            }
        }
        return reference
    }

    boolean supportsCollection(String collection) {
        return supportedDataTypes.get(collection) != null
    }
}
