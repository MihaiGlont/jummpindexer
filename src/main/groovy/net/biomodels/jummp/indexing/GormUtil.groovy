/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.biomodels.jummp.indexing

import grails.orm.bootstrap.HibernateDatastoreSpringInitializer
import groovy.transform.CompileStatic
import net.biomodels.jummp.annotationstore.*
import net.biomodels.jummp.indexing.support.ReadCommittedConnectionCustomiser
import net.biomodels.jummp.model.*
import net.biomodels.jummp.qcinfo.*
import net.biomodels.jummp.plugins.security.*
import org.hibernate.Session
import org.hibernate.SessionFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationContext
import org.springframework.orm.hibernate4.SessionHolder
import org.springframework.transaction.support.TransactionSynchronizationManager

import javax.sql.DataSource
import com.mchange.v2.c3p0.ComboPooledDataSource

/**
 * Utility class providing convenience methods for interacting with the data source.
 *
 * @author raza
 * @author Mihai Glonț
 */
@CompileStatic
class GormUtil {
    private static final Logger log = LoggerFactory.getLogger(GormUtil.class)
    private static final boolean IS_DEBUG_ENABLED = log.isDebugEnabled()
    public static final int BATCH_SIZE = 30
    public static final int MAX_POOL_SIZE = 50
    static boolean isTestEnvironment = false
    static ApplicationContext appCtx
    static final Map mysqlProperties = [
        // logging
        //logger: "com.mysql.jdbc.log.Slf4JLogger",
        //useUsageAdvisor: true,

        rewriteBatchedStatements: true, // need this to get the MySQL driver to actually use batching!
        // https://dev.mysql.com/doc/connector-j/5.1/en/connector-j-useconfigs.html
        cachePrepStmts: true,
        cacheCallableStmts: true,
        cacheServerConfiguration: true,
        useLocalSessionState: true,
        elideSetAutoCommits: true,
        alwaysSendSetIsolation: false,
        enableQueryTimeouts: false
    ]

    static void initGorm(RequestContext ctx) {
        if (IS_DEBUG_ENABLED) {
            log.debug "Started GORM bootstrapping..."
        }
        List classes = [Model,
                        ModelFormat,
                        Publication,
                        PublicationLinkProvider,
                        PublicationPerson,
                        RepositoryFile,
                        Revision,
                        Qualifier,
                        Statement,
                        ElementAnnotation,
                        RevisionAnnotation,
                        ResourceReference,
                        Role,
                        Person,
                        User,
                        UserRole,
                        QcInfo,
                        IndexingPlan,
                        Flag,
                        ModelFlag,
                        ModelElementType,
                        Tag,
                        ModelTag,
                        ModellingApproach
        ]
        Properties props = [
            "hibernate.hbm2ddl.auto": isTestEnvironment ? "create-drop" : "update",
            "hibernate.jdbc.batch_size": BATCH_SIZE,
            "hibernate.order_inserts": true,
            "hibernate.order_updates": true,
            "hibernate.jdbc.batch_versioned_data": true
        ] as Properties
        HibernateDatastoreSpringInitializer init =
                new HibernateDatastoreSpringInitializer(props, classes)
        DataSource dataSource = createDataSource(ctx)
        appCtx = init.configureForDataSource(dataSource)
        if (IS_DEBUG_ENABLED) {
            log.debug "...finished bootstrapping GORM."
        }
    }

    private static DataSource createDataSource(RequestContext ctx) {
        String url = ctx.databaseURL
        String username = ctx.databaseUsername
        String pwd = ctx.databasePassword
        boolean haveDatabaseDetails = url && username

        if (!haveDatabaseDetails) {
            final String path = ctx.configFilePath
            Properties jummpProps = new Properties()
            Reader reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(path), "UTF-8"))
            try {
                jummpProps.load(reader)
            } finally {
                reader?.close()
            }
            String dbType = jummpProps.getProperty("jummp.database.type")
            String protocol = dbType.toLowerCase()
            String server = jummpProps.getProperty("jummp.database.server")
            String port = jummpProps.getProperty("jummp.database.port")
            String dbName = jummpProps.getProperty("jummp.database.database")
            url = "jdbc:$protocol://$server:$port/$dbName".toString()
            username = jummpProps.getProperty("jummp.database.username")
            pwd = jummpProps.getProperty("jummp.database.password")
        }

        def ds = new ComboPooledDataSource()
        if (url.startsWith("jdbc:mysql")) {
            String joinedDbSettings = mysqlProperties.entrySet().join('&')
            if (url.endsWith('?')) {
                url = "$url$joinedDbSettings"
            } else if (url.contains('?')) {
                url = "$url&$joinedDbSettings"
            } else {
                url = "$url?${mysqlProperties.entrySet().join('&')}"
            }
        }
        ds.jdbcUrl = url
        ds.user = username
        ds.password = pwd
        ds.minPoolSize = 5
        ds.connectionCustomizerClassName = ReadCommittedConnectionCustomiser.class.name
        ds.maxPoolSize = MAX_POOL_SIZE
        // this should be the number of *distinct* frequently-used PreparedStatements
        // total size of StatementCache will be maxPoolSize * maxStatementsPerConnection
        ds.maxStatementsPerConnection = 15
        ds
    }

    static void openSession() {
        // Grails expects a SessionHolder proxy around the actual Hibernate session
        SessionHolder sessionHolder = new SessionHolder(sessionFactory.openSession())
        if (!isSessionAlreadyBound()) {
            TransactionSynchronizationManager.bindResource(sessionFactory, sessionHolder)
        }
    }

    static void closeSession() {
        if (!sessionFactory || !isSessionAlreadyBound()) {
            return
        }
        SessionHolder sessionHolder  = (SessionHolder) TransactionSynchronizationManager.
                getResource(sessionFactory)
        Session session = sessionHolder?.session
        if (session) {
            try {
                session.flush()
            } catch (Exception ignore) {
                // Hibernate will complain if we flush after there was a problem elsewhere
                log.debug("Failed to flush Hibernate session: {}", ignore.message)
            }
            session.close()
        }
        TransactionSynchronizationManager.unbindResourceIfPossible(sessionFactory)
        // remove references to previous transaction to avoid memory leak
        sessionHolder.clear()
    }

    static boolean isSessionAlreadyBound() {
        TransactionSynchronizationManager.getResource(sessionFactory) != null
    }

    static SessionFactory getSessionFactory () {
        appCtx?.getBean "sessionFactory", SessionFactory.class
    }

    static <T> T getBean(String name, Class<T> expectedType) {
        appCtx?.getBean(name, expectedType)
    }
}
