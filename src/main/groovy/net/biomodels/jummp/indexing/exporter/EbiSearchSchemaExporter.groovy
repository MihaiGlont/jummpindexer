/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 */

package net.biomodels.jummp.indexing.exporter

import grails.gorm.DetachedCriteria
import groovy.xml.MarkupBuilder
import groovyx.gpars.GParsPool
import net.biomodels.jummp.core.model.ModelState
import net.biomodels.jummp.indexing.RequestContext
import net.biomodels.jummp.model.Model
import net.biomodels.jummp.model.ModelTag
import net.biomodels.jummp.model.Revision
import net.biomodels.jummp.model.Tag
import net.biomodels.jummp.utils.ExporterUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.LinkedBlockingQueue

/**
 * Implementation of ModelExporter to export models for EBI Search.
 *
 * This class provides a means of exporting models to EBI Search entries to expose them to EBI Search.
 * It is the concrete implementation of ModelExporter contract.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tung.nguyen@ebi.ac.uk>
 */
class EbiSearchSchemaExporter extends ModelExporter<EbiSearchEntry> {

    static final Logger LOGGER = LoggerFactory.getLogger(EbiSearchSchemaExporter.class)

    private List<Revision> getAllAutoGenModels() {
        def autoGenModels = new DetachedCriteria(ModelTag).build {
            tag {
                eq "name", "Auto-Generated"
            }
            model { eq "deleted", false }
            projections {
                property "model"
            }
        }

        def revisions = []
        Revision.withTransaction {
            revisions = Revision.createCriteria().list {
                inList "model", autoGenModels.list()
                max "revisionNumber"
                distinct "model"
            }
        }

        Map<String, Revision> entries = new ConcurrentHashMap<String, Revision>()
        GParsPool.withPool {
            revisions.eachParallel { Revision revision ->
                Revision.withTransaction {
                    String submissionId = Revision.get(revision.id).model.submissionId
                    if (entries.containsKey(submissionId)) {
                        Revision rev = entries.get(submissionId)
                        if (revision.state == ModelState.PUBLISHED &&
                            revision.revisionNumber > rev.revisionNumber) {
                            entries.put(submissionId, revision)
                        }
                    } else {
                        if (revision.state == ModelState.PUBLISHED) {
                            entries.put(submissionId, revision)
                        }
                    }

                }
            }
        }
        return entries.values() as List<Revision>
    }

    @Override
    List<Revision> selectModelRevisionsForExport(List<Model> models) {
        return null
    }

    @Override
    List fetchDataSetEntriesFromDatabase(RequestContext ctx, Set<String> tagsExcluded) {
        List<EbiSearchEntry> entries = new ArrayList<EbiSearchEntry>()
        List<Revision> allModels = getAllAutoGenModels()
        List<Revision> models = new ArrayList<>()
        Model.withTransaction {
            List<Tag> excludedTags = Tag.findAllByNameInList(tagsExcluded as List)
            List<ModelTag> modelTags = ModelTag.findAllByTagInList(excludedTags)
            List revisions = modelTags.collect { it.model.revisions.collect { it.id} }
            models = allModels.findAll { Revision revision ->
                !revisions.contains(revision.id)
            }
        }
        Long count = models?.size()

        if (models?.size() > 0) {
            LOGGER.info("There are ${models.size()} revisions selected to export.")
            if (models.size() > 0) {
                // parallel
                entries = convertToListOfDataSetEntries(models, ctx)
            }
        }
        LOGGER.info("Total: ${count.toString()}")
        return entries
    }

    @Override
    List convertToListOfDataSetEntries(List<Revision> revisions, RequestContext ctx) {
        LOGGER.info("Converting revisions to EBI Search datasets...")
        def entries = new LinkedBlockingQueue<EbiSearchEntry>(revisions.size())

        GParsPool.withPool {
            revisions.eachParallel { Revision detachedRevision ->
                Model.withTransaction {
                    Revision r = Revision.get(detachedRevision.id)
                    Model m = r.model
                    String entryId = m.publicationId ?: m.submissionId
                    LOGGER.info("...Processing revision ${r.id} of model ${entryId}")
                    EbiSearchEntryBuilder builder
                    builder = new EbiSearchEntryBuilder()
                        .setRequestContext(ctx)
                        .setModel(m)
                        .setRevision(r)
                        .setFieldsInMandatorySection()
                    if (r.state == ModelState.PUBLISHED) {
                        builder.tokeniseName()
                            .setModelFields()
                            .setSubmitterInfo()
                            .setPublicationDetails()
                            .setQCInfo()
                            .setFieldsDerivedFromAnnotations()
                    }
                    EbiSearchEntry e = builder.build()
                    entries.add(e)
                }
            }
        }
        return entries.toList()
    }

    @Override
    String buildSchemaXmlAsString(List<EbiSearchEntry> modelEntries) {
        LOGGER.info("Building the string object representing EBI Search XML content.")
        def stringWriter = new StringWriter()
        def markupBuilder = new MarkupBuilder(stringWriter)
        markupBuilder.setDoubleQuotes(true)
        markupBuilder.mkp.xmlDeclaration(version: "1.0", encoding: "utf-8")

        String _name = "BioModels"//grailsApplication.config.jummp.metadata.officialDatabaseName
        String _description = """\
BioModels is a repository of mathematical models of biological processes.
            Models described from literature are manually curated and enriched with cross-references.
            """
        //grailsApplication.config.jummp.metadata.officialDatabaseDescription
        byte _releaseVersion = 1
        Date _releaseDate = new Date()
        int _entryCount = modelEntries.size() ?: 0
        markupBuilder.database {
            // add the principal information of database into MarkupBuilder object
            name(_name)
            description(_description)
            release(_releaseVersion)
            release_date(_releaseDate)
            entry_count(_entryCount)
            // add each model into MarkupBuilder object
            entries {
                setOmitEmptyAttributes(true)
                setOmitNullAttributes(true)
                for (EbiSearchEntry e: modelEntries) {
                    entry(id: e.id) {
                        name(e.name)
                        if (e.description) {
                            description(e.description)
                        }
                        dates() {
                            if (e.dates.containsKey("submission")) {
                                if (e.dates.get("submission")) {
                                    date(type: "submission", value:
                                        ExporterUtils.formatDateForOmicsDI(e.dates.get("submission")))
                                }
                            }
                            if (e.dates.containsKey("publication")) {
                                if (e.dates.get("publication")) {
                                    date(type: "publication", value:
                                        ExporterUtils.formatDateForOmicsDI(e.dates.get("publication")))
                                }
                            }

                            if (e.dates.containsKey("modification")) {
                                if (e.dates.get("modification")) {
                                    date(type: "last_modification", value:
                                        ExporterUtils.formatDateForOmicsDI(e.dates.get("modification")))
                                }
                            }
                        }

                        additional_fields() {
                            if (e.submitterName) {
                                field(name: "submitter", e.submitterName)
                            }
                            if (e.submitterMail) {
                                field(name: "submitter_mail", e.submitterMail)
                            }
                            if (e.submitterAffiliation) {
                                field(name: "submitter_affiliation", e.submitterAffiliation)
                            }
                            if (e.repositoryName) {
                                field(name: "repository", e.repositoryName)
                            }
                            if (e.fullDataSetLink) {
                                field(name: "full_dataset_link", e.fullDataSetLink)
                            }
                            if (e.publication) {
                                field(name: "publication", e.publication)
                            }
                            if (e.diseaseNames) {
                                e.diseaseNames.each {
                                    field(name: "disease", it)
                                }
                            }
                            for (def f: e.flags) {
                                field(name: "modelFlag", f)
                            }
                            for (def t: e.tags) {
                                field(name: "modelTag", t)
                            }
                            e.modellingApproaches?.each {
                                if (it)
                                    field(name: "modellingApproach", it)
                            }
                            if (e.modelFormat) {
                                field(name: "modelFormat", e.modelFormat)
                            }
                            if (e.submissionId) {
                                field(name: "submissionId", e.submissionId)
                            }
                            if (e.publicationId) {
                                field(name: "publicationId", e.publicationId)
                            }
                            if (e.publicationYear) {
                                field(name: "publication_year", e.publicationYear)
                            }
                            if (e.levelVersion) {
                                field(name: "levelVersion", e.levelVersion)
                            }

                            if (e.elementName) {
                                field(name: "elementName", e.elementName)
                            }
                            if (e.elementId) {
                                field(name: "elementId", e.elementId)
                            }
                            if (e.elementDescription) {
                                field(name: "elementDescription", e.elementDescription)
                            }
                            if (e.sbmlSBOTerm) {
                                field(name: "sbmlSBOTerm", e.sbmlSBOTerm)
                            }
                            if (e.curators) {
                                field(name: "curators", e.curators)
                            }
                            if (e.isPrivate) {
                                field(name: "isPrivate", e.isPrivate)
                            }
                            StringBuilder xrefsNotUsedForIsDerivedFromAnno = e.nonIsDerivedFromBioModelsAnnotations?.inject(
                                new StringBuilder(), { StringBuilder result, Map.Entry entry ->
                                result.append("${entry.key} ${entry.value} ")
                            })
                            if (xrefsNotUsedForIsDerivedFromAnno) {
                                def trimmed = xrefsNotUsedForIsDerivedFromAnno.subSequence(0,
                                    xrefsNotUsedForIsDerivedFromAnno.length() - 1)
                                field(name: "non_derived_xrefs", trimmed)
                            }

                            if (e.authors) {
                                def allAuthors = e.authors.join(', ')
                                String firstAuthor = e.authors.first()
                                field(name: "publication_authors", allAuthors)
                                field(name: "first_author", firstAuthor)
                            }
                            if (e.tokenisedName) {
                                field(name: "tokenised_name", e.tokenisedName)
                            }
                        }
                        if (!e.crossReferences?.isEmpty()) {
                            cross_references() {
                                for (xref in e.crossReferences) {
                                    ref(dbkey: xref.key, dbname: xref.value)
                                }
                            }
                        }
                    }
                }
            }
        }
        def content = stringWriter.toString()
        return content
    }
}
