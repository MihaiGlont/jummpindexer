/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing.exporter

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.annotationstore.RevisionAnnotation
import net.biomodels.jummp.annotationstore.Statement
import net.biomodels.jummp.core.model.ModelState
import net.biomodels.jummp.indexing.AnnotationReferenceResolver
import net.biomodels.jummp.indexing.RequestContext
import net.biomodels.jummp.model.*
import net.biomodels.jummp.utils.ExporterUtils
import org.apache.commons.lang.StringEscapeUtils
import org.apache.commons.lang.time.DateUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Builder of handling how to build OmicsDI entries.
 *
 * This class is the mean of building OmicsDI entries based on Builder Design Pattern.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tnguyen@ebi.ac.uk>
 */
@CompileStatic
class OmicsdiDataSetEntryBuilder {
    String id
    String name
    String description
    Map<String, Date> dates
    String tokenisedName
    String submitterName
    String submitterMail
    String submitterAffiliation
    String fullDataSetLink
    String publication
    Integer publicationYear
    String repositoryName
    List diseaseNames
    List modellingApproaches
    Map<String, String> nonIsDerivedFromBioModelsAnnotations
    String omicsType
    String dataProtocol
    String sampleProtocol
    String technologyType
    String curationStatus
    String modelFormat
    String submissionId
    String publicationId
    String levelVersion
    String validationStatus
    String certificationComment
    String curators
    String isPrivate
    List authors
    List flags
    List tags
    Map<String, String> crossReferences

    private RequestContext ctx
    private Model model
    private Revision revision

    static final Logger logger = LoggerFactory.getLogger(OmicsdiDataSetEntryBuilder.class)

    OmicsdiDataSetEntryBuilder setRequestContext(RequestContext ctx) {
        this.ctx = ctx
        return this
    }

    OmicsdiDataSetEntryBuilder setModel(Model model) {
        this.model = model
        return this
    }

    OmicsdiDataSetEntryBuilder setRevision(Revision revision) {
        this.revision = revision
        return this
    }

    OmicsdiDataSetEntryBuilder setFieldsInMandatorySection(String id, String name,
                                                           String description, Map<String, Date> dates) {
        this.id = id
        this.name = name
        this.description = description
        this.dates = dates
        return this
    }

    OmicsdiDataSetEntryBuilder setFieldsInMandatorySection() {
        this.id = model.publicationId ?: model.submissionId
        this.dates = [:]
        def originalRevision = model.revisions.first()
        if (revision.state == ModelState.PUBLISHED) {
            this.name = revision.name
            String description = revision.description ?: "No description"
            description = StringEscapeUtils.unescapeHtml(description).replaceAll("\\<.*?\\>", "")
            if (description.contains("\r\n")) {
                description = description.replaceAll("\\r\\n", System.lineSeparator())
            }
            this.description = description
            this.dates.put("submission", originalRevision.uploadDate)
            this.dates.put("publication", model.firstPublished)
            this.dates.put("modification", revision.uploadDate)
            this.isPrivate = "false"
        } else {
            // TODO: differentiate RELEASE and UNPUBLISHED
            Date datePublished = DateUtils.addYears(new Date(), 3)
            this.dates.put("publication", datePublished)
            this.name = this.id
            // TODO: externalise this property so that it could be used in jummp and indexer
            this.description = """\
 A model with this identifier has been submitted to BioModels, but has not yet been released."""
            this.isPrivate = "true"
        }
        return this
    }

    OmicsdiDataSetEntryBuilder tokeniseName() {
        this.tokenisedName = ExporterUtils.tokenise(name)
        return this
    }

    OmicsdiDataSetEntryBuilder setOmicsDiFieldsDerivedFromAnnotations() {
        List<Statement> statements = getAnnotationStatementsForRevision(revision)
        // cross reference fields
        Map<String, String> crossReferences = new LinkedHashMap<String, String>()
        Map<String, String> nonIsDerivedFromAnnotations = new LinkedHashMap<>()
        def diseases = []
        def modellingApproaches = []
        if (!statements?.isEmpty()) {
            logger.info("""\
...Converting all annotations of the revision ${revision.id} (model: ${model.submissionId}) to cross references""")
            final String I_ORG = "://identifiers.org/"
            for (Statement statement: statements) {
                boolean haveIsDerivedFromStatement = "http://biomodels.net/model-qualifiers/isDerivedFrom" == statement.qualifier.uri
                ResourceReference object = statement.object
                String dbkey = object.accession
                String dbname = object.datatype
                if (object.uri?.contains(I_ORG)) {
                    String[] parts = ExporterUtils.resolver.extractParts(object.uri, AnnotationReferenceResolver.AnnoType.IDENTIFIER)
                    dbname = parts[0]
                    dbkey = parts[1]
                }
                if (dbkey && dbname) {
                    if (!haveIsDerivedFromStatement && dbname == "biomodels.db") {
                        nonIsDerivedFromAnnotations[dbkey] = dbname
                    } else {
                        if (ExporterUtils.supportedDiseaseOntologies.get(dbname))
                            diseases << object.name ?: dbkey
                        else if (ExporterUtils.supportedModellingApproaches.get(dbkey))
                            modellingApproaches << object.name ?: dbkey
                        else
                            crossReferences.put(dbkey, dbname)
                    }
                } else {
                    logger.warn("Unable to export annotation statement ${statement.id} (xref #${object.id})")
                }
            }
        }
        this.nonIsDerivedFromBioModelsAnnotations = nonIsDerivedFromAnnotations
        this.crossReferences = crossReferences
        this.diseaseNames = diseases
        this.modellingApproaches = modellingApproaches
        return this
    }

    @CompileDynamic
    private static List<Statement> getAnnotationStatementsForRevision(Revision r) {
        RevisionAnnotation.executeQuery("""\
select s
from RevisionAnnotation ra
join ra.elementAnnotation ea
join ea.statement s
where ra.revision.id = ${r.id}
""")
    }

    OmicsdiDataSetEntryBuilder setSubmitterInfo() {
        Revision originalRevision = model.revisions.first()
        this.submitterName = originalRevision.owner.person.userRealName
        this.submitterMail = originalRevision.owner.email
        this.submitterAffiliation = originalRevision.owner.person.institution
        return this
    }

    OmicsdiDataSetEntryBuilder setPublicationDetails() {
        // the publication's authors -- get from Person
        this.authors = []
        Publication publication = model.publication
        if (publication) {
            String pub = """
                            ${publication.synopsis}. ${publication.issue}, ${publication.volume}.
                            ${publication.affiliation}"""
            this.authors = getManuscriptAuthors(publication)
            this.publicationYear = publication.year
            this.publication = pub
        }
        return this
    }

    @CompileDynamic
    private List<String> getManuscriptAuthors(Publication publication) {
        PublicationPerson.findAllByPublication(publication, [sort: 'position'])
            .collect { PublicationPerson it -> it.person.userRealName }
    }

    OmicsdiDataSetEntryBuilder setOmicsFeaturedFields() {
        this.repositoryName = ctx.repositoryName
        this.omicsType = "Models"
        return this
    }

    OmicsdiDataSetEntryBuilder setQCInfo() {
        this.validationStatus = revision.validationReport ?: ""
        String curationComment = revision.qcInfo?.comment ?: ""
        this.dataProtocol = curationComment
        this.sampleProtocol = curationComment
        this.technologyType = curationComment
        this.certificationComment = curationComment
        String curationStatus = model.publicationId?.trim() ? "Manually curated" :
            model.submissionId.startsWith("BMID") ? "Auto-generated" : "Non-curated"
        this.curationStatus = curationStatus
        return this
    }

    OmicsdiDataSetEntryBuilder setModelFields() {
        this.submissionId = model.submissionId
        this.publicationId = model?.publicationId ?: ""
        String entryId = model.publicationId ?: model.submissionId
        String fullDataSetLink = "https://www.ebi.ac.uk/biomodels/$entryId"
        this.fullDataSetLink = fullDataSetLink
        this.modelFormat = revision.format?.name ?: "Unknown"
        ModelFormat modelFormat = revision.format
        this.levelVersion = modelFormat?.formatVersion ?: ""

        Set<ModelFlag> modelFlags = model.flags
        List<String> flagLabels = modelFlags.collect {
            it.flag.label
        }
        this.flags = flagLabels
        this.tags = ExporterUtils.getTagsFromModel(model)
        return this
    }

    OmicsdiDataSetEntry build() {
        return new OmicsdiDataSetEntry(this)
    }
}
