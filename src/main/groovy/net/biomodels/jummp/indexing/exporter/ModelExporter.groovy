/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing.exporter

import groovy.transform.CompileStatic
import net.biomodels.jummp.indexing.ExportType
import net.biomodels.jummp.indexing.RequestContext
import net.biomodels.jummp.model.Model
import net.biomodels.jummp.model.Revision
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Export metadata, annotations of model to a certain specification.
 *
 * This class is the mean of exporting models to OmicsDI or EBI Search Specification.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tnguyen@ebi.ac.uk>
 */
@CompileStatic
abstract class ModelExporter<T> {
    private static final Logger logger = LoggerFactory.getLogger(ModelExporter.class)
    /**
     * This is the factory method to be used for creating a right instance at runtime.
     *
     * @param exportType An enum value indicating the type of entries we should export, i.e., OmicsDIXML or EBISearch
     * @return  an instance of a given concrete implementation of ModelExporter
     */
    static ModelExporter create(final ExportType exportType) {
        if (!exportType) {
            return null
        }
        switch (exportType) {
            case ExportType.OMICS_DI:
                return new OmicsdiXmlBasedSchemaExporter()
            case ExportType.EBI_SEARCH:
                return new EbiSearchSchemaExporter()
            default:
                throw new UnsupportedOperationException()
        }
    }
    abstract List<Revision> selectModelRevisionsForExport(List<Model> models)
    abstract List fetchDataSetEntriesFromDatabase(RequestContext ctx, Set<String> tagsExcluded)
    abstract List convertToListOfDataSetEntries(List<Revision> revisions, RequestContext ctx)
    abstract String buildSchemaXmlAsString(List<T> entries)

    /**
     * Build an XML based schema file containing a list of entries.
     *
     * This method aims to build an XML file. Its content is the string of entries
     * under XML format.
     *
     * @param filePath a string representing the file path
     * @param fileName a string representing the file name
     * @param entries a list of entries
     * @return true/false
     */
    boolean writeSchemaXml2File(String filePath, String fileName, List<T> entries) {
        if (filePath.charAt(filePath.length() - 1) != File.separatorChar) {
            filePath += File.separatorChar
        }
        String pathname = "${filePath}${fileName}"
        String content = this.buildSchemaXmlAsString(entries)
        File file = new File(pathname)
        logger.info("Building XML file containing EBI Search entries.")
        file.withWriter("utf-8") { writer ->
            writer.write(content)
            if (writer != null) {
                logger.info("Saved the XML file successfully.")
            }
        }
        return file.exists()
    }
}
