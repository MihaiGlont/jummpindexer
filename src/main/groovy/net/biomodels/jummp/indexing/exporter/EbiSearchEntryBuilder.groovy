package net.biomodels.jummp.indexing.exporter

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.core.model.ModelState
import net.biomodels.jummp.indexing.AnnotationReferenceResolver
import net.biomodels.jummp.indexing.RequestContext
import net.biomodels.jummp.indexing.SbmlUtils
import net.biomodels.jummp.model.Model
import net.biomodels.jummp.model.ModelFlag
import net.biomodels.jummp.model.ModelFormat
import net.biomodels.jummp.model.Publication
import net.biomodels.jummp.model.PublicationPerson
import net.biomodels.jummp.model.Revision
import net.biomodels.jummp.utils.ExporterUtils
import org.apache.commons.lang.StringEscapeUtils
import org.apache.commons.lang.time.DateUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Builder of handling how to build EBI Search entries.
 *
 * This class is the mean of building EBI Search entries based on Builder Design Pattern.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tnguyen@ebi.ac.uk>
 */
@CompileStatic
class EbiSearchEntryBuilder {
    String id
    String name
    String description
    Map<String, Date> dates
    String tokenisedName
    String submitterName
    String submitterMail
    String submitterAffiliation
    String fullDataSetLink
    String publication
    Integer publicationYear
    String repositoryName
    List diseaseNames
    List modellingApproaches
    Map<String, String> nonIsDerivedFromBioModelsAnnotations
    String curationStatus
    String modelFormat
    String levelVersion
    String submissionId
    String publicationId
    String curators
    String isPrivate
    List authors
    List flags
    List tags
    Map<String, String> crossReferences

    private RequestContext ctx
    private Model model
    private Revision revision

    static final Logger logger = LoggerFactory.getLogger(EbiSearchEntryBuilder.class)

    EbiSearchEntryBuilder setRequestContext(RequestContext ctx) {
        this.ctx = ctx
        return this
    }

    EbiSearchEntryBuilder setModel(Model model) {
        this.model = model
        return this
    }

    EbiSearchEntryBuilder setRevision(Revision revision) {
        this.revision = revision
        return this
    }

    EbiSearchEntryBuilder setFieldsInMandatorySection() {
        this.id = model.publicationId ?: model.submissionId
        this.dates = [:]
        this.repositoryName = ctx.repositoryName
        def originalRevision = model.revisions.first()
        if (revision.state == ModelState.PUBLISHED) {
            this.name = revision.name
            String description = revision.description ?: "No description"
            description = StringEscapeUtils.unescapeHtml(description).replaceAll("\\<.*?\\>", "")
            if (description.contains("\r\n")) {
                description = description.replaceAll("\\r\\n", System.lineSeparator())
            }
            this.description = description
            this.dates.put("submission", originalRevision.uploadDate)
            this.dates.put("publication", model.firstPublished)
            this.dates.put("modification", revision.uploadDate)
            this.isPrivate = "false"
        } else {
            // TODO: differentiate RELEASE and UNPUBLISHED
            Date datePublished = DateUtils.addYears(new Date(), 3)
            this.dates.put("publication", datePublished)
            this.name = this.id
            // TODO: externalise this property so that it could be used in jummp and indexer
            this.description = """\
A model with this identifier has been submitted to BioModels, but has not yet been released."""
            this.isPrivate = "true"
        }
        return this
    }

    EbiSearchEntryBuilder tokeniseName() {
        this.tokenisedName = ExporterUtils.tokenise(name)
        return this
    }

    EbiSearchEntryBuilder setSubmitterInfo() {
        Revision originalRevision = model.revisions.first()
        this.submitterName = originalRevision.owner.person.userRealName
        this.submitterMail = originalRevision.owner.email
        this.submitterAffiliation = originalRevision.owner.person.institution
        return this
    }

    EbiSearchEntryBuilder setPublicationDetails() {
        // the publication's authors -- get from Person
        this.authors = []
        Publication publication = model.publication
        if (publication) {
            String pub = """
                            ${publication.synopsis}. ${publication.issue}, ${publication.volume}.
                            ${publication.affiliation}"""
            this.authors = getManuscriptAuthors(publication)
            this.publicationYear = publication.year
            this.publication = pub
        }
        return this
    }

    EbiSearchEntryBuilder setQCInfo() {
        String curationComment = revision.qcInfo?.comment ?: ""
        String curationStatus = "Auto-generated"
        this.curationStatus = curationStatus
        return this
    }

    EbiSearchEntryBuilder setModelFields() {
        this.submissionId = model.submissionId
        this.publicationId = model?.publicationId ?: ""
        String entryId = model.publicationId ?: model.submissionId
        String fullDataSetLink = "https://www.ebi.ac.uk/biomodels/$entryId"
        this.fullDataSetLink = fullDataSetLink
        this.modelFormat = revision.format?.name ?: "Unknown"
        ModelFormat modelFormat = revision.format
        this.levelVersion = modelFormat?.formatVersion ?: ""

        Set<ModelFlag> modelFlags = model.flags
        List<String> flagLabels = modelFlags.collect {
            it.flag.label
        }
        this.flags = flagLabels
        this.tags = ExporterUtils.getTagsFromModel(model)
        return this
    }

    @CompileDynamic
    private List<String> getManuscriptAuthors(Publication publication) {
        PublicationPerson.findAllByPublication(publication, [sort: 'position'])
            .collect { PublicationPerson it -> it.person.userRealName }
    }

    @CompileDynamic
    EbiSearchEntryBuilder setFieldsDerivedFromAnnotations() {
        String cfgFilePath = ctx.configFilePath
        AnnotationReferenceResolver.initialiseWithProperties(cfgFilePath)
        Properties jummpProperties = new Properties()
        jummpProperties.load(new FileInputStream(cfgFilePath))
        String workingDirectory = jummpProperties.get("jummp.vcs.workingDirectory")
        String modelFolder = revision.model.vcsIdentifier
        String modelFileName = ExporterUtils.getModelFileName(revision)
        String modelFilePath = "${workingDirectory}${File.separator}${modelFolder}${File.separator}${modelFileName}"
        org.sbml.jsbml.Model sbmlModel = SbmlUtils.parseSbmlModel(modelFilePath)
        Map<String, Set<String>> annotations = SbmlUtils.getAllAnnotationsWithQualifiers(sbmlModel)
        // cross reference fields
        Map<String, String> crossReferences = new LinkedHashMap<String, String>()
        Map<String, String> nonIsDerivedFromAnnotations = new LinkedHashMap<>()
        def diseases = []
        def modellingApproaches = []
        if (!annotations?.isEmpty()) {
            logger.info("""\
...Converting all annotations of the revision ${revision.id} (model: ${model.submissionId}) to cross references""")
            final String I_ORG = "://identifiers.org/"
            final String URN_MIRIAM = "urn:miriam"
            AnnotationReferenceResolver resolver = AnnotationReferenceResolver.instance()
            for (String qualifier : annotations.keySet()) {
                boolean haveIsDerivedFromStatement = "isDerivedFrom" == qualifier
                String dbkey = ""
                String dbname = ""
                Set<String> resources = annotations.get(qualifier)
                for (String annotation: resources) {
                    if (annotation.contains(I_ORG)) {
                        String[] parts = resolver.extractParts(annotation, AnnotationReferenceResolver.AnnoType
                            .IDENTIFIER)
                        dbname = parts[0]
                        dbkey = parts[1]
                    } else if (annotation.contains(URN_MIRIAM)) {
                        String[] parts = resolver.extractParts(annotation, AnnotationReferenceResolver.AnnoType.MIRIAM)
                        dbname = parts[0]
                        dbkey = parts[1]
                    }
                    if (dbkey && dbname) {
                        if (!haveIsDerivedFromStatement && dbname == "biomodels.db") {
                            nonIsDerivedFromAnnotations[dbkey] = dbname
                        } else {
                            if (ExporterUtils.supportedDiseaseOntologies.get(dbname)) {
                                /*ResourceReference reference = resolver.resolve(dbkey, dbname)
                                String resolvedName = reference.name
                                diseases << resolvedName*/
                                diseases << dbkey
                            } else if (ExporterUtils.supportedModellingApproaches.get(dbkey)) {
                                /*ResourceReference reference = resolver.resolve(dbkey, dbname)
                                String resolvedName = reference.name
                                modellingApproaches << resolvedName*/
                                modellingApproaches << dbkey
                            } else
                                crossReferences.put(dbkey, dbname)
                        }
                    } else {
                        logger.warn("Unable to export annotation ${annotation}")
                    }
                }
            }
        }
        this.nonIsDerivedFromBioModelsAnnotations = nonIsDerivedFromAnnotations
        this.crossReferences = crossReferences
        this.diseaseNames = diseases
        this.modellingApproaches = modellingApproaches
        return this
    }

    EbiSearchEntry build() {
        return new EbiSearchEntry(this)
    }
}
