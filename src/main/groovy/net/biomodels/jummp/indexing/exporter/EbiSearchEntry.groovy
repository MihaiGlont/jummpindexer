/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 */

package net.biomodels.jummp.indexing.exporter

import groovy.transform.CompileStatic

/**
 * @short EbiSearchEntry class for handling  EBI Search specification's entry fields.
 *
 * This class provides means of managing the fields for each entry as described in
 * EBI Search specification. See more details about this specification:
 *      XML dump file: https://www.ebi.ac.uk/seqdb/confluence/display/EXT/XML+dump+file
 *      JSON format: https://www.ebi.ac.uk/seqdb/confluence/display/EXT/JSON+data+format
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tung.nguyen@ebi.ac.uk>
 */
@CompileStatic
class EbiSearchEntry {
    String id
    String name
    String description
    Map<String, Date> dates
    /*
     * No Lucene Analyser can deal with the BioModels format for the model name <Author><Year>-Keyword1_KeywordN
     * We use a dedicated field to enhance partial matches of the model name.
     */
    String tokenisedName
    String submitterName
    String submitterMail
    String submitterAffiliation
    String fullDataSetLink
    String publication
    Integer publicationYear
    String repositoryName
    List diseaseNames
    List modellingApproaches
    /**
     * OmicsDI tracks dataset reuse by analysing cross references from the same repository. For BioModels, this means
     * that an annotation having a BioModels entry as both its subject and its object implies that the subject (i.e. the
     * model with that annotation) is derived from the model denoted by the cross reference (object) of said annotation.
     * The problem with this approach is that it ignores the qualifier (i.e. the predicate of the annotation).
     *
     * We use the isDerivedFrom model qualifier to track model reuse/repurposing. This means that annotations with model
     * qualifiers like 'is' or 'isVersionOf' should not be included in the cross references map below so as to not
     * confuse OmicsDI into treating them as indications of dataset reuse.
     */
    Map<String, String> nonIsDerivedFromBioModelsAnnotations
    String curationStatus
    String modelFormat
    String submissionId
    String publicationId
    String levelVersion
    String elementId
    String elementName
    String elementDescription
    String sbmlSBOTerm
    String curators
    List authors
    List flags
    List tags
    String isPrivate

    /* cross reference fields, excluding BioModels references used in isDerivedFrom annotations */
    Map<String, String> crossReferences

    EbiSearchEntry(EbiSearchEntryBuilder builder) {
        // compulsory fields (mandatory section)
        this.id = builder.id
        this.name = builder.name
        this.description = builder.description
        this.dates = builder.dates

        // repository/database and omics type field (omics specified fields)
        this.repositoryName = builder.repositoryName

        // tokenised name field
        this.tokenisedName = builder.tokenisedName

        // model fields
        this.submissionId = builder.submissionId
        this.publicationId = builder.publicationId
        this.fullDataSetLink = builder.fullDataSetLink
        this.modelFormat = builder.modelFormat
        this.levelVersion = builder.levelVersion
        this.flags = builder.flags
        this.tags = builder.tags
        if (builder.isPrivate == "true") {
            this.isPrivate = builder.isPrivate
        }

        // submitter fields
        this.submitterName = builder.submitterName
        this.submitterMail = builder.submitterMail
        this.submitterAffiliation = builder.submitterAffiliation

        // publication details fields
        this.authors = builder.authors
        this.publication = builder.publication
        this.publicationYear = builder.publicationYear

        // qc fields
        this.curationStatus = builder.curationStatus

        // annotations derived fields
        this.nonIsDerivedFromBioModelsAnnotations = builder.nonIsDerivedFromBioModelsAnnotations
        this.crossReferences = builder.crossReferences
        this.diseaseNames = builder.diseaseNames
        this.modellingApproaches = builder.modellingApproaches
    }
}
