/**
 * Copyright (C) 2010-2017 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing

import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.indexing.miriam.MiriamRegistryService
import org.codehaus.groovy.runtime.IOGroovyMethods

/**
 * @author tnguyen@ebi.ac.uk on 02/05/17.
 */
class InterProDatabaseFetcher extends DatabaseBasedAnnoProcessor {
    protected static final Map<String, String> entryList = new HashMap<String, String>()
    static def supportedDataTypes = ["interpro": "INTERPRO"]

    ResourceReference getTermInformation(ResourceReference reference) {
        String accession = reference.getAccession()
        String collType = reference.getDatatype()
        if (!cached.containsKey(accession) && !loadFromDatabaseIntoCache(accession, collType)) {
            if (!entryList.isEmpty()) {
                String name = entryList.get(accession)
                if (name) {
                    reference.setName(name)
                }
            }
            String collection = MiriamRegistryService.fetchCollectionNameByType(collType)
            reference.setCollectionName(collection)
            saveResource(reference)
        } else {
            Long cachedXrefId = cached.get(accession)
            reference = ResourceReference.get(cachedXrefId)
            if (!reference) {
                logger.error("Could not obtain term information about xref {} ({})", accession, collType)
            }
        }
        return reference
    }

    boolean supportsCollection(String collection) {
        return supportedDataTypes.get(collection) != null
    }

    void initialiseDataTypes() {
        URL urlObj = new URL("ftp://ftp.ebi.ac.uk/pub/databases/interpro/entry.list")
        try {
            IOGroovyMethods.eachLine urlObj.openStream(), "UTF-8", { String entry ->
                String[] parts = entry.split(" ", 2)
                if (parts.length == 2) {
                    entryList.put(parts[0], parts[1])
                }
            }
        } catch (IOException e) {
            logger.error("I/O error while loading InterPro entry list: {}", e.message)
        }
    }
}
