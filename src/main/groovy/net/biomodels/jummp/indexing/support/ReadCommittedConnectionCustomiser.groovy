package net.biomodels.jummp.indexing.support

import com.mchange.v2.c3p0.AbstractConnectionCustomizer
import groovy.transform.CompileStatic

import java.sql.Connection

/**
 * Simple class used to set the transaction isolation level for all data source connections to
 * READ_COMMITTED.
 *
 * @see {@linkplain net.biomodels.jummp.indexing.GormUtil}
 *
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 */
@CompileStatic
class ReadCommittedConnectionCustomiser extends AbstractConnectionCustomizer {

    public void onAcquire(Connection c, String parentDataSourceIdentityToken ) throws
            Exception {
        c.transactionIsolation = Connection.TRANSACTION_READ_COMMITTED
    }
}
