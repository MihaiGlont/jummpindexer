/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package net.biomodels.jummp.indexing

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import uk.ac.ebi.pride.utilities.ols.web.service.client.OLSClient
import uk.ac.ebi.pride.utilities.ols.web.service.config.OLSWsConfigProd
import uk.ac.ebi.pride.utilities.ols.web.service.model.Ontology

/**
 *
 * @author raza
 */
abstract class OLSBasedAnnoProcessor extends ResourceReferenceResolver implements AncestryProvider,
        SynonymProvider, TermInformationProvider {

    protected static final Logger logger = LoggerFactory.getLogger(OLSBasedAnnoProcessor.class)
    // The OLS ontologies supported to resolve cross references names in this class are initialised
    Map<String, String> supportedTypes = new LinkedHashMap<String, String>()
    OLSClient olsClient = new OLSClient(new OLSWsConfigProd())

    OLSBasedAnnoProcessor() {
        initialiseDataTypes()
    }

    boolean supportsAncestry() {
        return true
    }

    boolean supportsSynonyms() {
        return true
    }

    boolean supportsCollection(String collection) {
        supportedTypes.get(collection)
    }

    protected void initialiseDataTypes() {
        List<Ontology> ontologies = olsClient.getOntologies()
        ontologies.each {
            supportedTypes.put(it.namespace, it.name)
        }
    }

    void shutdown() {

    }
}

