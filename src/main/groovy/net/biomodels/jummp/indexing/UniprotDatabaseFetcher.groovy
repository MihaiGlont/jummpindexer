/**
 * Copyright (C) 2010-2017 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing

import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.indexing.miriam.MiriamRegistryService
import org.xml.sax.SAXException

/**
 * @author tnguyen@ebi.ac.uk on 24/04/17.
 */
class UniprotDatabaseFetcher extends DatabaseBasedAnnoProcessor {
    static def supportedDataTypes = ["uniprot": "UNIPROT",
                                     "taxonomy": "TAXONOMY",
                                     "ec-code": "EC-CODE"]

    ResourceReference getTermInformation(ResourceReference reference) {
        String accession = reference.getAccession()
        String collType = reference.getDatatype()
        String url  = ""
        String name = ""
        def response
        if (!cached.containsKey(accession) && !loadFromDatabaseIntoCache(accession, collType)) {
            if (collType.equalsIgnoreCase("uniprot")) {
                url = "http://www.uniprot.org/uniprot/${accession}.xml"
                try {
                    String r = new URL(url).text
                    if (r) {
                        response = new XmlSlurper().parseText(r)
                    } else {
                        logger.warn("Could not resolve UniProt accession $accession")
                    }
                } catch (Exception e) {
                    logger.error("Failed to resolve UniProt term {}: {}", accession, e.toString())
                }
                /**
                 * Pay attention to the XML structure of UniProt returned value
                 * At this time implementing this method, a UniProt document has entry as the root tag.
                 * Because we need its full name, we need to point to fullName tag directly.
                 */
                def protein = response?.entry?.protein
                name = protein?.recommendedName?.fullName?.text()
                if (!name) {
                    name = protein?.submittedName?.text()
                }
            } else if (collType.equalsIgnoreCase("taxonomy")) {
                url = "http://www.uniprot.org/uniprot/?query=taxonomy:${accession}&columns=id,organism&format=xml&sort=score&limit=1"
                try {
                    response = new XmlSlurper().parse(url)
                } catch (IOException | SAXException e) {
                    logger.error("Failed to resolve Taxonomy term {} using UniProt: {} ", accession,
                            e.toString())
                }
                name = response?.entry?.organism?.name?.find { it.@scientific }
            } else if (collType.equalsIgnoreCase("ec-code")) {
                try {
                    url = "http://www.uniprot.org/uniprot/?query=ec:${accession}&limit=1&format=xml"
                    response = new XmlSlurper().parse(url)
                } catch (IOException | SAXException e) {
                    logger.error("Failed to resolve EC code {}: {}", accession, e.toString())
                }
                name = response?.entry?.protein?.recommendedName?.fullName?.text()
                if (!name) {
                    name = response?.entry?.protein?.submittedName?.text()
                }
            }
            if (name) {
                reference.setName(name)
            }
            String collection = MiriamRegistryService.fetchCollectionNameByType(collType)
            reference.setCollectionName(collection)
            saveResource(reference)
        } else {
            Long cachedXrefId = cached.get(accession)
            reference = ResourceReference.get(cachedXrefId)
            if (!reference) {
                logger.error("Could not obtain term information about xref {} ({})", accession, collType)
            }
        }

        return reference
    }

    boolean supportsCollection(String collection) {
        return supportedDataTypes.get(collection) != null
    }
}
