/**
 * Copyright (C) 2010-2017 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing

import net.biomodels.jummp.annotationstore.ResourceReference
import net.biomodels.jummp.indexing.miriam.MiriamRegistryService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import uk.ac.ebi.pride.utilities.ols.web.service.client.OLSClient
import uk.ac.ebi.pride.utilities.ols.web.service.config.OLSWsConfigProd
import uk.ac.ebi.pride.utilities.ols.web.service.model.Identifier
import uk.ac.ebi.pride.utilities.ols.web.service.model.Term

/**
 * @author tnguyen@ebi.ac.uk on 21/04/17.
 */
class OLSDirectFetcher extends OLSBasedAnnoProcessor {
    private static final Logger logger = LoggerFactory.getLogger(OLSDirectFetcher.class)

    void getAncestors(ResourceReference reference) {
        // Ancestors get loaded with term information
    }

    void getSynonyms(ResourceReference reference) {
        // Synonyms get loaded with term information
    }

    ResourceReference getTermInformation(ResourceReference reference) {
        String id = reference.getAccession()
        String collType = reference.getDatatype()
        if (!cached.containsKey(id) && !loadFromDatabaseIntoCache(id, collType)) {
            Identifier termIdentifier = new Identifier(id, Identifier.IdentifierType.OBO)
            Term term
            try {
                term = olsClient.getTermById(termIdentifier, collType)
                if (term.getAnnotation().containsAnnotation("term replaced by")) {
                    String replacedByUrl = term.getAnnotation().getAnnotation("term replaced by").get(0)
                    replacedByUrl = replacedByUrl.substring(replacedByUrl.lastIndexOf("/") + 1)
                    replacedByUrl = replacedByUrl.replace("_", ":")
                    termIdentifier.setIdentifier(replacedByUrl)
                    term = olsClient.getTermById(termIdentifier, collType)
                }

                if (term) {
                    String label = term.label
                    String[] des = term.description
                    reference.setName(label)
                    if (des) {
                        reference.setDescription(des[0])
                    }
                    String[] synonyms = term.getSynonyms()
                    if (synonyms) {
                        reference.synonyms.addAll(synonyms)
                    }
                    //List<Term> parents = olsClient.getTermParents(termIdentifier, term.ontologyPrefix, Integer.MAX_VALUE)
                }
            } catch (Exception e) {
                logger.error("OLS API returned an error while resolving $id from $collType", e.message)
            }
            String collection = MiriamRegistryService.fetchCollectionNameByType(collType)
            reference.setCollectionName(collection)
            saveResource(reference)
        }
        else {
            Long cachedXrefId = cached.get(id)
            reference = ResourceReference.get(cachedXrefId)
            if (!reference) {
                logger.error("Could not obtain term information about xref {} ({})", id, collType)
            }
        }
        return reference
    }
}
