/**
 * Copyright (C) 2010-2016 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing

import groovy.transform.CompileStatic
import net.biomodels.jummp.indexing.exporter.ModelExporter
import net.biomodels.jummp.utils.ExporterUtils
import net.biomodels.jummp.model.Revision
import org.hibernate.FlushMode
import org.hibernate.Session
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Parse the request to choose a right direction of processing requested job
 *
 * Replying on the parameters passed to its instance, it will either index a given model
 * or export OmicsDI XML files
 *
 * @author raza
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tung.nguyen@ebi.ac.uk>
 */
class RequestParser {
    static final Logger log = LoggerFactory.getLogger(RequestParser.class)
    static final boolean IS_DEBUG_ENABLED = log.isDebugEnabled()

    @CompileStatic
    static void handleRequest(String jsonPath) {
        RequestContext ctx = new RequestContext(jsonPath)
        String uniqueId = ctx.partialData?.uniqueId
        if (!ctx.isEmpty()) //noinspection GroovyMissingReturnStatement
        {
            String cfgPath = ctx.configFilePath
            GormUtil.initGorm(ctx)
            AnnotationReferenceResolver.initialiseWithProperties(cfgPath)
            ModelIndexer indexer = ModelIndexerFactory.createIndexer(ctx)
            if (IS_DEBUG_ENABLED) {
                log.debug("Begin data extraction process...")
            }
            // extract file content and persist annotations into database
            GormUtil.openSession()
            Revision.withSession { Session session ->
                // disable Hibernate's defensive auto flushing to avoid StaleObjectStateExceptions
                // when persisting revision annotations.
                FlushMode flushMode = session.flushMode
                session.flushMode = FlushMode.MANUAL
                try {
                    indexer.extractFileContent(ctx)
                }
                catch (Exception e) {
                    String docPath = ctx.indexFile.absolutePath
                    log.error("Exception indexing $uniqueId -- ${docPath} - ${e.message}", e)
                } finally {
                    session.flushMode = flushMode
                }
            }
            GormUtil.closeSession()
            if (IS_DEBUG_ENABLED) {
                log.debug("Finished data extraction process for $uniqueId.")
                log.debug("Shutting down AnnotationReferenceResolver for $uniqueId.")
            }
            AnnotationReferenceResolver.instance().shutdown()
            if (IS_DEBUG_ENABLED) {
                log.debug("Finished shutting down AnnotationReferenceResolver for $uniqueId.")
            }

            if (IS_DEBUG_ENABLED) {
                log.debug "Begin indexing the content extracted from $uniqueId"
            }
            // put extracted information in search index
            indexer.indexData(ctx)

            if (IS_DEBUG_ENABLED) {
                log.debug "Finished indexing the data for $uniqueId."
            }
        }
    }

    @CompileStatic
    static void generateMetadataForSearchServer(String jsonPath, ExportType exportType) {
        RequestContext ctx = new RequestContext(jsonPath)
        if (!ctx.isEmpty()) {
            def time = System.nanoTime()
            GormUtil.initGorm(ctx)
            ModelExporter exporter = ModelExporter.create(exportType)
            Set tagsExcluded = ctx.omicsdiExportOptions.get("tagsExcluded") as Set
            List entries = exporter.fetchDataSetEntriesFromDatabase(ctx, tagsExcluded)
            if (IS_DEBUG_ENABLED) {
                log.debug("......inside exporting files.")
            }
            String filePath = ctx.folderPath
            String dateTimeString = new Date().format("yyyyMMdd-HHmmss")
            int batchSize = entries.size()
            boolean allowMultipleFiles = ctx.omicsdiExportOptions?.get("allowMultipleFiles")
            if (allowMultipleFiles) {
                batchSize = (Integer) ctx.omicsdiExportOptions?.get("numberEntriesOnEachFile")
            }
            double times = Math.ceil((double) entries.size()/batchSize)
            int length = (int) (Math.log10(times) + 1);
            int offset = 0
            String fileName
            // cannot access exportType.fileNamePrefix until move to Groovy 2.5.7+ (GROOVY-9007)
            final String fileNamePrefix = exportType.getFileNamePrefix()
            for (int i = 1; i <= times; ++i) {
                List batchEntries = ExporterUtils.partition(entries, offset, batchSize)
                if (times == 1) {
                    fileName = "${fileNamePrefix}-${dateTimeString}.xml"
                } else {
                    String suffix = String.format("%0${length}d", i)
                    fileName = "${fileNamePrefix}-${dateTimeString}-${suffix}.xml"
                }
                boolean succeeded = exporter.writeSchemaXml2File(filePath, fileName, batchEntries)
                if (succeeded) {
                    if (IS_DEBUG_ENABLED) {
                        log.debug("File named ${fileName} has been created successfully.")
                    }
                } else {
                    log.error("File file named ${fileName} cannot be created due to errors.")
                }
                offset += batchSize
            }
            time = (System.nanoTime() - time) / 1000000.0
            log.debug("It took ${time}ms to generate the bundle of XML file(s).")
        } else {
            log.error("Cannot parse and extract data from the configuration file ${jsonPath}")
        }
    }
}
