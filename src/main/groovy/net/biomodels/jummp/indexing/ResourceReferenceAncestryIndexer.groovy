/**
 * Copyright (C) 2010-2015 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.indexing

import net.biomodels.jummp.annotationstore.ResourceReference

/**
 * Strategy for extracting external information about terms coming from hierarchical
 * constructs such as ontologies or taxonomies.
 *
 * Implementations of this interface are expected to look up terms based the supplied key and
 * to use the resulting ancestry
 *      to construct appropriate ResourceReference objects in the database AND
 *      to populate the appropriate fields (e.g. externalReference(Name|ID)) of the supplied
 *      request context.
 *
 * @author Mihai Glonț <mglont@ebi.ac.uk>
 */
interface ResourceReferenceAncestryIndexer {
    /**
     * Finds a ResourceReference for the given cross reference.
     *
     * @param xref the cross reference to look up
     * @param ctx the context of the current request
     * @param fieldName the name of the field where information about this xref should be added
     * @param existingAnnotations the current RDF Statement that is being added to the SolrDoc
     * @return the corresponding ResourceReference
     */
    ResourceReference indexAncestors(String xref, RequestContext ctx, String fieldName,
            Map<String, Collection<String>> existingAnnotations)
}
