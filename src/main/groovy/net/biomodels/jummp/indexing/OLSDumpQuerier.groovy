/**
 * Copyright (C) 2010-2017 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/





package net.biomodels.jummp.indexing

import groovy.sql.Sql
import net.biomodels.jummp.annotationstore.ResourceReference
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 *
 * @author raza
 * @author tnguyen@ebi.ac.uk
 */
class OLSDumpQuerier extends OLSBasedAnnoProcessor {
    Sql sql
    private static final Logger logger = LoggerFactory.getLogger(OLSDumpQuerier.class)

    OLSDumpQuerier(String server,
                          String port,
                          String database,
                          String username,
                          String password) {
        sql = Sql.newInstance("jdbc:mysql://${server}:${port}/${database}", username,
                              password, "com.mysql.jdbc.Driver")
        initialiseDataTypes()
    }

    void getAncestors(ResourceReference reference) {
        // Ancestors get loaded with term information
    }

    void getSynonyms(ResourceReference reference) {
        // Synonyms get loaded with term information
    }

    private void addSynonyms(String termID, ResourceReference reference) {
        String query = "select synonym_value from term_synonym where term_pk='${termID}'"
            sql.eachRow(query) {
                reference.addSynonym(it.synonym_value)
        }
    }

    ResourceReference getTermInformation(ResourceReference reference) {
        String id = reference.getAccession()
        String collType = reference.getDatatype()
        if (!cached.containsKey(id) && !loadFromDatabaseIntoCache(id, collType)) {
            String query = "SELECT term.* FROM term, ontology where shortName='${collType.toUpperCase()}' and identifier='${id}' and term.ontology_id = ontology.ontology_id"
            def row = sql.firstRow(query)
            if (row) {
                reference.setName(row.term_name)
                reference.setDescription(row.definition)
                String termID = row.term_pk
                addSynonyms(termID, reference)
                addParents(termID, reference)
            }
            saveResource(reference)
        }
        else {
            Long cachedXrefId = cached.get(id)
            reference = ResourceReference.get(cachedXrefId)
            if (!reference) {
                logger.error("Could not obtain term information about xref {} ({})", id, collType)
            }
        }
        return reference
    }

    private void addParents(String termID, ResourceReference ref) {
        Map<String, ResourceReference> processQueue = new HashMap<>()
        processQueue.put(termID, ref)
        String collType = ref.getDatatype()
        while (!processQueue.isEmpty()) {
            def nextQueue = [:]
            processQueue.each { termIdentifier, resource ->
                String query = "SELECT term.* FROM term, term_relationship where subject_term_pk='${termIdentifier}' and object_term_pk = term_pk"
                sql.eachRow(query) {
                    String parentID = it.identifier
                    if (!cached.containsKey(parentID) && !loadFromDatabaseIntoCache(parentID, collType)) {
                        ResourceReference parent = new ResourceReference(accession: parentID,
                                                                         datatype: collType,
                                                                         uri: "http://identifiers.org/${collType}/${parentID}",
                                                                         name: it.term_name,
                                                                         description: it.definition)
                        String parentTermID = it.term_pk
                        addSynonyms(parentTermID, parent)
                        nextQueue.put(parentTermID, parent)
                        saveResource(parent)
                    }
                    Long parentXrefId = cached.get(parentID)
                    ResourceReference parent = ResourceReference.get(parentXrefId)
                    if (!parent) {
                        logger.error "Cannot get parent {} of term {} attached to the session",
                                parentID, termID
                        return
                    }
                    resource.addParent(parent)
                }
                saveResource(resource)
            }
            processQueue.clear()
            processQueue.putAll(nextQueue)
        }
    }

    void initialiseDataTypes() {
        if (sql) {
            sql.eachRow( 'select shortName from ontology' ) {
                supportedTypes.add(it.shortName)
            }
        }
    }

    void shutdown() {
        sql?.close()
    }
}

