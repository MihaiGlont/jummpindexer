/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.orgin/licenses/agpl-3.0.html>.
 */


package net.biomodels.jummp.utils

import net.biomodels.jummp.indexing.AnnotationReferenceResolver
import net.biomodels.jummp.model.Model
import net.biomodels.jummp.model.RepositoryFile
import net.biomodels.jummp.model.Revision

/**
 * Collecting util functions being used for model exporters
 *
 * This class aims at gathering useful/helper functions being used in exporting models.
 *
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @author Tung Nguyen <tnguyen@ebi.ac.uk>
 */
class ExporterUtils {
    static final AnnotationReferenceResolver resolver = new AnnotationReferenceResolver()
    static final Map<String, String> supportedDiseaseOntologies =
        ["doid": "Human Disease Ontology",
         "icd" : "International Classification of Diseases",
         "ido" : "Infectious Disease Ontology",
         "omim": "Online Mendelian Inheritance in Man"
        ]
    static final Map<String, String> supportedModellingApproaches =
        ["MAMO_0000046": "Ordinary differential equation (ODE) model",
         "MAMO_0000041": "Bayesian model",
         "MAMO_0000030": "Logical model",
         "MAMO_0000025": "Petri net model",
         "MAMO_0000024": "Agent-based model",
         "MAMO_0000022": "Rule-based model",
         "MAMO_0000009": "Constraint-based model"
        ]

    static String tokenise(String name) {
        name?.replaceAll('[-_]', ' ')
    }

    /**
     * Converts a timestamp to an ISO8601 date (e.g. 2001-12-31)
     *
     * @param date the date to format
     *
     * @return the formatted date
     */
    static String formatDateForOmicsDI(Date date) {
        date?.format "yyyy-MM-dd"
    }

    /**
     * Extract a sub list against the larger list of objects replied on the offset position and
     * step size (i.e. the number of elements beginning from the offset position forwards.
     *
     * @param entries List of elements
     * @param offset a positive integer denoting the starting point where we begin partitioning elements
     * @param batchSize a positive integer denoting the amount of elements which are extracted to the new list
     *                  since the offset point
     * @return a list   List of elements
     */
    static List partition(List entries, int offset, int batchSize) {
        List dataSetEntries = []
        offset = offset < 0 ? 0 : offset
        int limit = offset + batchSize;
        if (limit > entries.size()) {
            limit = entries.size()
        }
        for (int index = offset; index < limit; index++) {
            dataSetEntries.add(entries.get(index));
        }
        return dataSetEntries
    }

    static List<String> getTagsFromModel(Model model) {
        List tags = []
        String query = "select t.name from ModelTag m join m.tag t where m.model.id = ${model.id}"
        tags = Model.executeQuery(query)
        return tags
    }

    static String getModelFileName(Revision revision) {
        RepositoryFile file = RepositoryFile.findAllByRevision(revision).find {
            it.mainFile
        }
        file?.path
    }
}
